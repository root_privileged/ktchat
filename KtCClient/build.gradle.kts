
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm")
    application
    id("org.openjfx.javafxplugin") version "0.0.7"
}

application {
    mainClass.set("flist.root.ktchat.KtCClient")
}

javafx {
    version = Cfg.UI.FXVersion
    modules = listOf("javafx.controls", "javafx.graphics")
}

repositories {
    jcenter()
    maven(url = "https://oss.sonatype.org/content/repositories/snapshots/")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    api(Cfg.CommonLibraries.Reactor.BOM)
    implementation(project(":KtCCore"))

    implementation(Cfg.UI.TornadoFX)
    implementation(Cfg.UI.MiglayoutFX)

    implementation(Cfg.Logging.KotlinLogging)
    implementation(Cfg.CommonLibraries.Reactor.Core)
    implementation(Cfg.CommonLibraries.Reactor.Extras)

    testImplementation(Cfg.Testing.Spek)
    testImplementation(Cfg.Testing.Kluent)

    testApi(Cfg.Testing.SpekJUnitRunner)
}

tasks {
    test {
        useJUnitPlatform {
            includeEngines("spek2")
        }
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
    }
}

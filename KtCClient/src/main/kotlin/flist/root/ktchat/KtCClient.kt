package flist.root.ktchat

import tornadofx.*

class MyApp: App(MyView::class)
class MyView: View() {
    override val root = vbox {
        button("Press me")
        label("Waiting") {
            vboxConstraints {

            }
        }


    }
}

object KtCClient {
    @JvmStatic
    fun main(args: Array<String>) {
        launch<MyApp>(args)
    }
}

import net.miginfocom.layout.BoundSize
import net.miginfocom.layout.ConstraintParser
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


class BoundSizeProperty(val setter: (BoundSize) -> Any, val isHor: Boolean, val isGap: Boolean = false): ReadOnlyProperty<Any?, BoundSize> {
    override fun getValue(thisRef: Any?, property: KProperty<*>): BoundSize {
        return BoundSize.NULL_SIZE
    }

    fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        setter(ConstraintParser.parseBoundSize(value, isGap, isHor))
    }

    operator fun plus(rawValue: String) {

    }

    operator fun setValue(layoutConstraintsBuilder: LayoutConstraintsBuilder, property: KProperty<*>, boundSize: BoundSize) {

    }
}

class DelegatedFieldProperty<in R, T>(val setter: (T) -> Any, val getter: () -> T) : ReadWriteProperty<R, T> {
    override fun getValue(thisRef: R, property: KProperty<*>): T {
        return getter()
    }

    override fun setValue(thisRef: R, property: KProperty<*>, value: T) {
        setter(value)
    }
}

class ToggledFieldProperty(val setter: (Boolean) -> Any, val toggledValue: Boolean = true) : ReadWriteProperty<Any?, Boolean> {
    override fun getValue(thisRef: Any?, property: KProperty<*>): Boolean {
        setter(toggledValue)
        return toggledValue
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: Boolean) {
        setter(value)
    }
}

import javafx.event.EventTarget
import javafx.scene.Node
import net.miginfocom.layout.*
import net.miginfocom.layout.ConstraintParser.*
import org.tbee.javafx.scene.layout.MigPane
import org.tbee.javafx.scene.layout.fxml.MigPane.setCc
import tornadofx.*
import java.lang.reflect.Field
import kotlin.reflect.jvm.javaField

object MigLayoutDSLExtensions {
    fun EventTarget.migPane(layoutConstraints: LC, op: MigPane.() -> Unit = {}) : MigPane {
        val migPane = MigPane(layoutConstraints)
        return opcr(this, migPane, op)
    }

    fun EventTarget.migPane(layoutConstraints: LC, columnConstraints: AC, op: MigPane.() -> Unit = {}) : MigPane {
        val migPane = MigPane(layoutConstraints, columnConstraints)
        return opcr(this, migPane, op)
    }

    fun EventTarget.migPane(layoutConstraints: LC, columnConstraints: AC, rowConstraints: AC, op: MigPane.() -> Unit = {}) : MigPane {
        val migPane = MigPane(layoutConstraints, columnConstraints, rowConstraints)
        return opcr(this, migPane, op)
    }

    fun EventTarget.migPane(layoutConstraints: String, op: MigPane.() -> Unit = {}) : MigPane {
        val migPane = MigPane(layoutConstraints)
        return opcr(this, migPane, op)
    }

    fun EventTarget.migPane(layoutConstraints: String, columnConstraints: String, op: MigPane.() -> Unit = {}) : MigPane {
        val migPane = MigPane(layoutConstraints, columnConstraints)
        return opcr(this, migPane, op)
    }

    fun EventTarget.migPane(layoutConstraints: String, columnConstraints: String, rowConstraints: String, op: MigPane.() -> Unit = {}) : MigPane {
        val migPane = MigPane(layoutConstraints, columnConstraints, rowConstraints)
        return opcr(this, migPane, op)
    }

    fun layout(block: LayoutConstraintsBuilder.() -> Unit): LC = LayoutConstraintsBuilder().apply(block).build()
    fun rows(block: AxisConstraintBuilder.() -> Unit): AC = AxisConstraintBuilder(false).apply(block).build()
    fun columns(block: AxisConstraintBuilder.() -> Unit): AC = AxisConstraintBuilder(true).apply(block).build()

    inline fun <T : Node> T.cc(op: (ComponentConstraintBuilder.() -> Unit)): T {
        val cc = ComponentConstraintBuilder().apply(op).build()
        setCc(this, cc)
        return this
    }
}


@DslMarker
annotation class MigLayoutDSL

@MigLayoutDSL
class LayoutConstraintsBuilder {
    private var lc = LC()

    val leftToRight: Boolean by ToggledFieldProperty(lc::setLeftToRight)
    val topToBottom: Boolean by ToggledFieldProperty(lc::setTopToBottom)
    var wrapAfter: Int by DelegatedFieldProperty(lc::setWrapAfter, lc::getWrapAfter)

    var wbound by BoundSizeProperty(lc::setWidth, true)

    var width: String = ""
        set(value) {
            lc.width = parseBoundSize(value, false, true)
        }

    var height: String = ""
        set(value) { lc.height = parseBoundSize(value, false, false) }

    var minWidth: String = ""
        set(value) {
            lc.minWidth(value)
            field = value
        }
    var maxWidth: String = ""
        set(value) {
            lc.maxWidth(value)
            field = value
        }

    var minHeight: String = ""
        set(value) {
            lc.minHeight(value)
            field = value
        }
    var maxHeight: String = ""
        set(value) {
            lc.maxHeight(value)
            field = value
        }

    var hideMode = 0
        set(value) {
            lc.hideMode = value
            field = value
        }

    var gapx: String = ""
        set(value) { lc.gridGapX = parseBoundSize(value, true, true)}
    var gapy: String = ""
        set(value) { lc.gridGapY = parseBoundSize(value, true, false)}

    var debugMilliseconds = 1000
        set(value) {
            lc.debugMillis = value
            field = value
        }
        get() {
            lc.debugMillis = field
            return field
        }

    val noGrid: Boolean
        get() {
            lc.isNoGrid = true
            return true
        }
    val noCache: Boolean
        get() {
            lc.isNoCache = true
            return true
        }
    val noVisualPadding: Boolean
        get() {
            lc.isVisualPadding = true
            return true
        }

    val fillX: Boolean
        get() {
            lc.isFillX = true
            return true
        }
    val fillY: Boolean
        get() {
            lc.isFillY = true
            return true
        }

    val flowY: Boolean
        get() {
            lc.isFlowX = false
            return true
        }

    var insets: String = ""
        set(value) {
            lc.insets = ConstraintParser.parseInsets(value, true)
        }

    var alignX: String = ""
        set(value) {
            lc.alignX = ConstraintParser.parseUnitValueOrAlign(value, true, null)
        }
    var alignY: String = ""
        set(value) {
            lc.alignY = ConstraintParser.parseUnitValueOrAlign(value, false, null)
        }

    var packWidthAlign = 0.5F
        set(value) {
            lc.packWidthAlign = value
            field = value
        }
    var packHeightAlign = 1.0F
        set(value) {
            lc.packHeightAlign = value
            field = value
        }

    var packWidth: String = ""
        set(value) {
            lc.packWidth = parseBoundSize(value, false, true)
        }

    var packHeight: String = ""
        set(value) {
            lc.packHeight = parseBoundSize(value, false, false)
        }

    fun build(): LC = lc
}

sealed class BoundType(val string: String = "", private val isHor: Boolean) {
    object Horizontal : BoundType(isHor = true)
    object Vertical : BoundType(isHor = false)
}


private operator fun BoundSize.plusAssign(s: String) {

}

@MigLayoutDSL
class DimConstraintBuilder(private val isColumns: Boolean) {
    private var dimConstraint = DimConstraint()

    var gapBefore: String = ""
        set(value) {
            dimConstraint.gapBefore = parseBoundSize(value, true, isColumns)
        }
    var gapAfter: String = ""
        set(value) {
            dimConstraint.gapAfter = parseBoundSize(value, true, isColumns)
        }

    val fill: Boolean
        get() {
            dimConstraint.isFill = true
            return true
        }

    val noGrid: Boolean
        get() {
            dimConstraint.isNoGrid = true
            return true
        }

    var sizeGroup: String = ""
        set(value) {
            dimConstraint.sizeGroup = value
            field = value
        }

    var shrinkPriority: Int = 0
        set(value) {
            dimConstraint.shrinkPriority = value
            field = value
        }

    var shrink: Float = 100F
        set(value) {
            dimConstraint.shrink = value
            field = value
        }

    var growPriority: Int = 0
        set(value) {
            dimConstraint.growPriority = value
            field = value
        }

    var grow: Float = 100F
        set(value) {
            dimConstraint.grow = value
            field = value
        }

    var align: String = ""
        set(value) {
            dimConstraint.align = parseUnitValueOrAlign(value, isColumns, null)
        }

    var size: String = ""
        set(value) {
            dimConstraint.size = parseBoundSize(value, false, isColumns)
        }

    fun build() : DimConstraint = dimConstraint
}

@MigLayoutDSL
class AxisConstraintBuilder(private val isColumns: Boolean) {
    var constraints: MutableList<DimConstraint> = ArrayList() //: AC = AC()

    fun col(block: DimConstraintBuilder.() -> Unit) {
        constraints.add(DimConstraintBuilder(isColumns).apply(block).build())
    }

    fun row(block: DimConstraintBuilder.() -> Unit) {
        constraints.add(DimConstraintBuilder(isColumns).apply(block).build())
    }

    fun build() : AC {
        return AC().also {

            it.constaints = constraints.toTypedArray()
        }
    }
}

@MigLayoutDSL
class ComponentConstraintBuilder {
    private var cc = CC()

    val dockNorth: Boolean
        get() {
            cc.dockSide = 0
            return true
        }

    val dockWest: Boolean
        get() {
            cc.dockSide = 1
            return true
        }

    val dockSouth: Boolean
        get() {
            cc.dockSide = 2
            return true
        }

    val dockEast: Boolean
        get() {
            cc.dockSide = 3
            return true
        }

    val dockCenter: Boolean
        get() {
            cc.horizontal.grow = 100F
            cc.vertical.grow = 100F
            cc.pushX = 100F
            cc.pushY = 100F
            return true
        }

    var newline: String
        get() {
            cc.isNewline = true
            return cc.newlineGapSize.toString()
        }
        set(value) {
            cc.newlineGapSize = parseBoundSize(value, true, true)
        }

    var wrap: String
        get() {
            cc.isWrap = true
            return cc.isWrap.toString()
        }
        set(value) {
            cc.wrapGapSize = parseBoundSize(value, true, true)
        }

    val flowY: Boolean
        get() {
            cc.flowY()
            return true
        }

    var skip: Int
        get() {
            cc.skip = 1
            return 1
        }
        set(value) {
            cc.skip = value
        }

    var split: Int
        get() {
            cc.split = LayoutUtil.INF
            return LayoutUtil.INF
        }
        set(value) {
            cc.split = value
        }

    var spanX: Int
        get() {
            cc.spanX = LayoutUtil.INF
            return cc.spanX
        }
        set(value) {
            cc.spanX = value
        }

    var spanY: Int
        get() {
            cc.spanY = LayoutUtil.INF
            return cc.spanY
        }
        set(value) {
            cc.spanY = value
        }

    var shrinkX: Float
        get() {
            cc.horizontal.shrink = 100F
            return 100F
        }
        set(value) {
            cc.horizontal.shrink = value
        }

    var shrinkY: Float
        get() {
            cc.vertical.shrink = 100F
            return 100F
        }
        set(value) {
            cc.vertical.shrink = value
        }

    var sizeGroupX: String
        set(value) {
            cc.horizontal.sizeGroup = value
        }
        get() = cc.horizontal.sizeGroup

    var sizeGroupY: String
        set(value) {
            cc.vertical.sizeGroup = value
        }
        get() = cc.vertical.sizeGroup

    var growX: Float
        get() {
            cc.horizontal.grow = 100F
            return cc.horizontal.grow
        }
        set(value) {
            cc.horizontal.grow = value
        }

    var growY: Float
        get() {
            cc.vertical.grow = 100F
            return cc.vertical.grow
        }
        set(value) {
            cc.vertical.grow = value
        }

    var gapTop: String = ""
        set(value) {
            cc.vertical.gapBefore = parseBoundSize(value, true, false)
        }

    var gapBottom: String = ""
        set(value) {
            cc.vertical.gapAfter = parseBoundSize(value, true, false)
        }

    var gapLeft: String = ""
        set(value) {
            cc.horizontal.gapBefore = parseBoundSize(value, true, true)
        }

    var gapRight: String = ""
        set(value) {
            cc.horizontal.gapAfter = parseBoundSize(value, true, true)
        }

    var alignX: String = ""
        set(value) {
            cc.horizontal.align =  parseUnitValueOrAlign(value, true, null)
        }

    var alignY: String = ""
        set(value) {
            cc.vertical.align = parseUnitValueOrAlign(value, false, null)
        }

    var pushX: Float
        get() {
            cc.pushX = 100F
            return cc.pushX
        }
        set(value) {
            cc.pushX = value
        }

    var pushY: Float
        get() {
            cc.pushY = 100F
            return cc.pushY
        }
        set(value) {
            cc.pushY = value
        }

    var tag: String
        get() {
            return cc.tag
        }
        set(value) {
            cc.tag = value
        }

    var hideMode: Int
        get() = cc.hideMode
        set(value) {
            cc.hideMode = value
        }

    var id: String
        get() = cc.id
        set(value) {
            cc.id = value
            val dIx = cc.id.indexOf('.')
            if (dIx == 0 || dIx == cc.id.length - 1)
                throw IllegalArgumentException("Dot must not be first or last!")

        }

    val external: Boolean
        get() {
            cc.isExternal = true
            return true
        }

    var width: String = ""
        set(value) {
            cc.horizontal.size = parseBoundSize(value, false, true)
        }
    var height: String = ""
        set(value) {
            cc.vertical.size = parseBoundSize(value, false, false)
        }

    var visualPadding: String
        get() = cc.visualPadding.toString()
        set(value) {
            val p = parseInsets(value, false)
            cc.visualPadding = arrayOf(p[0], if (p.size > 1) p[1] else null, if (p.size > 2) p[2] else null, if (p.size > 3) p[3] else null)
        }

    var pad: String
        get() = cc.padding.toString()
        set(value) {
            val p = parseInsets(value, false)
            cc.padding = arrayOf(p[0], if (p.size > 1) p[1] else null, if (p.size > 2) p[2] else null, if (p.size > 3) p[3] else null)
        }

    var endGroupX: String
        get() = cc.horizontal.endGroup
        set(value) {
            cc.horizontal.endGroup = value
        }

    var endGroupY: String
        get() = cc.vertical.endGroup
        set(value) {
            cc.vertical.endGroup = value
        }

    fun cell(x: Int, y: Int) {
        cellX = x
        cellY = y
    }

    var cellX: Int
        get() = cc.cellX
        set(value) {
            cc.cellX = value
        }

    var cellY: Int
        get() = cc.cellY
        set(value) {
            cc.cellY = value
        }

    fun boundedPosition(x: String = "", y: String = "", x2: String = "", y2: String = "") {
        if (!cc.isBoundsInGrid) {
            throw IllegalArgumentException("Cannot combine 'position' with 'x/y/x2/y2' keywords.");
        }

        val coordinates = arrayOf(
                parseUnitValue(x, true),
                parseUnitValue(y, false),
                parseUnitValue(x2, true),
                parseUnitValue(y2, false)
        )

        cc.pos = coordinates
    }

    fun position(x: String = "", y: String = "", x2: String = "", y2: String = "") {
        if (cc.pos != null && cc.isBoundsInGrid)
            throw IllegalArgumentException("Can not combine 'pos' with 'x/y/x2/y2' keywords.");

        val coordinates = arrayOf(
                parseUnitValue(x, true),
                parseUnitValue(y, false),
                parseUnitValue(x2, true),
                parseUnitValue(y2, false)
        )

        if (coordinates[0] == null && coordinates[2] == null || coordinates[1] == null && coordinates[3] == null)
            throw IllegalArgumentException("Both x and x2 or y and y2 can not be null!")

        cc.pos = coordinates

        // TODO: hacky but no other way to do this.
        val ff = cc::javaClass.javaField?.get("boundsInGrid") as Field
        ff.isAccessible = true
        ff.setBoolean(cc, false)
        //cc.isBoundsInGrid = false
    }

    fun bounds(wMin: String = "", wMax: String = "", hMin: String = "", hMax: String = "") {
        val dcH: DimConstraint = cc.horizontal
        val dcV: DimConstraint = cc.vertical

        val width: Pair<UnitValue, UnitValue> =
                Pair(parseUnitValue(wMin, true) ?: dcH.size.min, parseUnitValue(wMax, true) ?: dcH.size.max)
        val height: Pair<UnitValue, UnitValue> =
                Pair(parseUnitValue(hMin, true) ?: dcV.size.min, parseUnitValue(hMax, true) ?: dcV.size.max)

        dcH.size = BoundSize(width.first, dcH.size.preferred, width.second, null)
        dcV.size = BoundSize(height.first, dcV.size.preferred, height.second, null)
    }

    fun build() = cc
}
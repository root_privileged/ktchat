import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm")
    application
}

application {
    mainClass.set("flist.root.ktchat.KtCScriptKt")
}

repositories {
    jcenter()
    maven( url = "https://oss.sonatype.org/content/repositories/snapshots/")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    api(Cfg.CommonLibraries.Reactor.BOM)
    implementation(project(":KtCCore"))

    implementation(Cfg.GraalJS)

    implementation(Cfg.Logging.KotlinLogging)
    implementation(Cfg.CommonLibraries.Reactor.Core)
    implementation(Cfg.CommonLibraries.Reactor.Extras)

    testImplementation(Cfg.Testing.Spek)
    testImplementation(Cfg.Testing.Kluent)

    testApi(Cfg.Testing.SpekJUnitRunner)
}

tasks {
    test {
        useJUnitPlatform {
            includeEngines("spek2")
        }
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
    }
}

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm")
    application
}

application {
    mainClass.set("flist.root.ktchat.KtCCoreKt")
}

repositories {
    jcenter()
    maven( url = "https://dl.bintray.com/hotkeytlt/maven")
    maven( url = "https://oss.sonatype.org/content/repositories/snapshots/")
}

dependencies {
    implementation(kotlin("stdlib-jdk8", DependencyVersions.ktVersion))
    implementation(kotlin("reflect", DependencyVersions.ktVersion))

    api(platform(Cfg.CommonLibraries.Reactor.BOM))
    api(Cfg.CommonLibraries.Reactor.Core)
    api(Cfg.CommonLibraries.Reactor.Extras)

    api(platform(Cfg.CommonLibraries.Jackson.BOM))
    api(Cfg.CommonLibraries.Jackson.Core)
    api(Cfg.CommonLibraries.Jackson.Databind)
    api(Cfg.CommonLibraries.Jackson.JSR310)
    api(Cfg.CommonLibraries.Jackson.Annotations)
    api(Cfg.CommonLibraries.Jackson.YAML)

    implementation(Cfg.Konfig)

    implementation(Cfg.Koin)
    implementation(Cfg.KoinExt)

    implementation(Cfg.BetterParse)

    implementation(Cfg.Web.JSoup)
    implementation(Cfg.Web.KTor)

    implementation(Cfg.Web.Scarlet)
    implementation(Cfg.Web.ScarletOkHttp)

    implementation(Cfg.Web.KTorWebsocket)
    implementation(Cfg.Web.KTorJackson)
    implementation(Cfg.Web.KTorOkHttp)
    //implementation(Cfg.Web.KTorApache)

    implementation(Cfg.Logging.KotlinLogging)
    implementation(Cfg.Logging.Log4J2Api)
    implementation(Cfg.Logging.Log4J2Core)
    implementation(Cfg.Logging.Log4J2Impl)

    implementation(Cfg.CommonLibraries.Reflections)
    implementation(Cfg.CommonLibraries.Guava)
    implementation(Cfg.CommonLibraries.Javassist)

    
    // Testing Stuff Below
    testImplementation(Cfg.Testing.Reactor)
    testImplementation(Cfg.Testing.Spek)
    testImplementation(Cfg.Testing.Kluent)
    testImplementation(Cfg.Testing.KTor)

    testApi(Cfg.Testing.SpekJUnitRunner)
}

tasks {
    test {
        useJUnitPlatform {
            includeEngines("spek2")
        }
    }
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
        kotlinOptions.freeCompilerArgs += "-Xuse-experimental=kotlin.Experimental"
    }
}

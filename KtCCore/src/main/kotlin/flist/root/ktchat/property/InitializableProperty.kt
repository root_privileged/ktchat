package flist.root.ktchat.property

import mu.KotlinLogging
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

typealias InitializerFunction<R, T> = (R) -> T

private val logger = KotlinLogging.logger {}

class InitProperty<in R, T>(
        private val defaultValue: T,
        private var initializer: InitializerFunction<R, T>,
        private var innerValue: T = defaultValue
) : ReadOnlyProperty<R, T> {

    fun invalidate() {
        innerValue = defaultValue
    }

    override fun getValue(thisRef: R, property: KProperty<*>): T {
        if(innerValue == defaultValue)
        {
            try {
                innerValue = initializer(thisRef)
                logger.trace { "Property $property.name updated with innerValue $innerValue" }
            }
            catch(ex: Exception) {
                logger.error { "Error getting innerValue for property ${property.name} in $thisRef caused by $ex" }
            }
        }

        return innerValue
    }
}
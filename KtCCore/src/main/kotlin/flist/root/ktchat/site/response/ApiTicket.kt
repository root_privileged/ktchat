package flist.root.ktchat.site.response


import com.fasterxml.jackson.annotation.JsonProperty
import io.ktor.client.HttpClient
import io.ktor.client.request.forms.submitForm
import io.ktor.client.request.parameter
import io.ktor.client.request.url

/***
 * Format Received via /json/getApiTicket.php
 *  POST Form Values
 *      - account
 *      - password
 *      - no_friends = true
 *      - no_bookmarks = true
 *      - new_character_list = true
 */
data class ApiTicket(
        @JsonProperty("characters")
        val characters: Map<String, Int> = HashMap(),
        @JsonProperty("default_character")
        val defaultCharacter: Int = 0,
        @JsonProperty("ticket")
        val ticket: String = ""
) : ApiResponse()

suspend fun HttpClient.getTicket(account: String, password: String) : ApiTicket {
    return submitForm {
        url("https://f-list.net/json/getApiTicket.php")

        parameter("account", account)
        parameter("password", password)
        parameter("no_friends", true)
        parameter("no_bookmarks", true)
        parameter("new_character_list", true)
    }
}
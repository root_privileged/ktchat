package flist.root.ktchat.site.response


import com.fasterxml.jackson.annotation.JsonProperty
import io.ktor.client.HttpClient
import io.ktor.client.request.forms.submitForm
import io.ktor.client.request.parameter
import io.ktor.client.request.url

/***
 * Format Received via /json/api/bookmark-list.php
 *  POST Form Values
 *      - account
 *      - ticket
 */
data class BookmarkList(
    @JsonProperty("characters")
    val characters: List<String>
) : ApiResponse()

suspend fun HttpClient.getBookmarkList(account: String, ticket: String) : BookmarkList {
    return submitForm {
        url("https://f-list.net/json/api/bookmark-list.php")

        parameter("account", account)
        parameter("ticket", ticket)
    }
}
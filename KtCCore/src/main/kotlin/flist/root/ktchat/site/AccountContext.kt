package flist.root.ktchat.site

import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.coroutines.MainEventScope
import flist.root.ktchat.di.ScopedKoinComponent
import flist.root.ktchat.di.inject
import flist.root.ktchat.property.InitProperty
import flist.root.ktchat.site.response.getTicket
import io.ktor.client.HttpClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.koin.core.Koin
import org.koin.core.context.GlobalContext
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope

private val logger = KotlinLogging.logger {}

class AccountContext(
        parent: Koin,
        val account: String,
        var password: String
) : ScopedKoinComponent, CoroutineScope by MainEventScope() {
    override val scope: Scope = parent.createScope("accountContext-$account", named("account"))

    init {
        scope.declare(this)
    }

    val client: HttpClient by inject()

    val ticket: String by InitProperty("", {
        runBlocking(it.coroutineContext) {
            val tkt =  client.getTicket(account, password)
            tkt.ticket
        }
    })


    fun openCharacter(name: String): ChatContext = scope.get { parametersOf(name) }


    companion object {
        fun open(acct: String, pass: String): AccountContext = GlobalContext.get().koin.get { parametersOf(acct, pass) }
    }
}
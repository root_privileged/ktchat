package flist.root.ktchat.site.response

import com.fasterxml.jackson.annotation.JsonProperty

abstract class ApiResponse(
        @JsonProperty("error")
        val error: String = ""
) {
    val valid: Boolean
        get() = error.isEmpty()
}
package flist.root.ktchat.site.response


import com.fasterxml.jackson.annotation.JsonProperty
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.url

/***
 * Format Received via /json/api/mapping-list.php
 *  GET
 */
data class MappingList(
    @JsonProperty("infotag_groups")
    val infotagGroups: List<InfotagGroup>,
    @JsonProperty("infotags")
    val infotags: List<Infotag>,
    @JsonProperty("kink_groups")
    val kinkGroups: List<KinkGroup>,
    @JsonProperty("kinks")
    val kinks: List<Kink>,
    @JsonProperty("listitems")
    val listitems: List<Listitems>
) : ApiResponse() {
    data class Kink(
        @JsonProperty("description")
        val description: String,
        @JsonProperty("group_id")
        val groupId: String,
        @JsonProperty("id")
        val id: String,
        @JsonProperty("character")
        val name: String
    )

    data class KinkGroup(
        @JsonProperty("id")
        val id: String,
        @JsonProperty("character")
        val name: String
    )

    data class Infotag(
        @JsonProperty("group_id")
        val groupId: String,
        @JsonProperty("id")
        val id: String,
        @JsonProperty("list")
        val list: String,
        @JsonProperty("character")
        val name: String,
        @JsonProperty("type")
        val type: String
    )

    data class Listitems(
        @JsonProperty("id")
        val id: String,
        @JsonProperty("character")
        val name: String,
        @JsonProperty("innerValue")
        val value: String
    )

    data class InfotagGroup(
        @JsonProperty("id")
        val id: String,
        @JsonProperty("character")
        val name: String
    )
}

suspend fun HttpClient.getMappingList(account: String, ticket: String) : MappingList {
    return get {
        url("https://f-list.net/json/api/friend-list.php")
    }
}
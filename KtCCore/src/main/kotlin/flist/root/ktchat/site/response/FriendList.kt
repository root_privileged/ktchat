package flist.root.ktchat.site.response


import com.fasterxml.jackson.annotation.JsonProperty
import io.ktor.client.HttpClient
import io.ktor.client.request.forms.submitForm
import io.ktor.client.request.parameter
import io.ktor.client.request.url

/***
 * Format Received via /json/api/friend-list.php
 *  POST Form Values
 *      - account
 *      - ticket
 */
data class FriendList(
    @JsonProperty("friends")
    val friends: List<Friend>
) : ApiResponse()

data class Friend(
        @JsonProperty("dest")
        val dest: String,
        @JsonProperty("last_online")
        val lastOnline: Int,
        @JsonProperty("source")
        val source: String
)

suspend fun HttpClient.getFriendList(account: String, ticket: String) : FriendList {
    return submitForm {
        url("https://f-list.net/json/api/friend-list.php")

        parameter("account", account)
        parameter("ticket", ticket)
    }
}
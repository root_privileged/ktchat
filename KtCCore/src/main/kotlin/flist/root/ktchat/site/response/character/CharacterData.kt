package flist.root.ktchat.site.response.character


import com.fasterxml.jackson.annotation.JsonProperty
import flist.root.ktchat.site.response.ApiResponse

data class CharacterData(
        @JsonProperty("badges")
        val badges: List<Any>,
        @JsonProperty("character_list")
        val characterList: List<Any>,
        @JsonProperty("created_at")
        val createdAt: Int,
        @JsonProperty("current_user")
        val currentUser: CurrentUser,
        @JsonProperty("custom_kinks")
        val customKinks: Map<Int, CustomKinkValue>,
        @JsonProperty("custom_title")
        val customTitle: String,
        @JsonProperty("customs_first")
        val customsFirst: Boolean,
        @JsonProperty("description")
        val description: String,
        @JsonProperty("id")
        val id: Int,
        @JsonProperty("images")
        val images: List<Image>,
        @JsonProperty("infotags")
        val infotags: Map<Int, String>,
        @JsonProperty("inlines")
        val inlines: Map<Int, InlineValue>,
        @JsonProperty("is_self")
        val isSelf: Boolean,
        @JsonProperty("kinks")
        val kinks: Map<Int, String>,
        @JsonProperty("character")
        val name: String,
        @JsonProperty("settings")
        val settings: Settings,
        @JsonProperty("timezone")
        val timezone: Int,
        @JsonProperty("updated_at")
        val updatedAt: Int,
        @JsonProperty("views")
        val views: Int
) : ApiResponse() {
    data class CustomKinkValue(
            @JsonProperty("children")
            val children: List<Any>,
            @JsonProperty("choice")
            val choice: String,
            @JsonProperty("description")
            val description: String,
            @JsonProperty("character")
            val name: String
    )

    data class CurrentUser(
            @JsonProperty("animated_icons")
            val animatedIcons: Boolean,
            @JsonProperty("inline_mode")
            val inlineMode: Int
    )

    data class Image(
            @JsonProperty("description")
            val description: String,
            @JsonProperty("extension")
            val extension: String,
            @JsonProperty("height")
            val height: String,
            @JsonProperty("image_id")
            val imageId: String,
            @JsonProperty("sort_order")
            val sortOrder: String,
            @JsonProperty("width")
            val width: String
    )

    data class Settings(
            @JsonProperty("public")
            val `public`: Boolean,
            @JsonProperty("customs_first")
            val customsFirst: Boolean,
            @JsonProperty("guestbook")
            val guestbook: Boolean,
            @JsonProperty("prevent_bookmarks")
            val preventBookmarks: Boolean,
            @JsonProperty("show_friends")
            val showFriends: Boolean
    )

    data class InlineValue(
            @JsonProperty("extension")
            val extension: String,
            @JsonProperty("hash")
            val hash: String,
            @JsonProperty("nsfw")
            val nsfw: Boolean
    )
}
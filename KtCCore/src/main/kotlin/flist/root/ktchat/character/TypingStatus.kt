package flist.root.ktchat.character

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider

enum class TypingStatus {
    Clear,
    Paused,
    Typing
}

class TypingStatusDeserializer : JsonDeserializer<TypingStatus>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): TypingStatus {
        return when (p?.valueAsString) {
            "clear" -> TypingStatus.Clear
            "paused" -> TypingStatus.Paused
            "typing" -> TypingStatus.Typing
            else -> TypingStatus.Clear
        }
    }
}

class TypingStatusSerializer : JsonSerializer<TypingStatus>() {
    override fun serialize(value: TypingStatus?, gen: JsonGenerator?, serializers: SerializerProvider?) {
        gen?.writeString(value?.name?.toLowerCase())
    }
}
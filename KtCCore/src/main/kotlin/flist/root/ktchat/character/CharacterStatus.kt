package flist.root.ktchat.character

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider

enum class CharacterStatus {
    Online,
    Looking,
    Away,
    Idle,
    Busy,
    DND;

    companion object {
        fun fromString(str: String) : CharacterStatus {
            return when (str) {
                "looking" -> Looking
                "busy" -> Busy
                "away" -> Away
                "idle" -> Idle
                "dnd" -> DND
                "online" -> Online
                else -> Online
            }
        }
    }
}

class CharacterStatusDeserializer : JsonDeserializer<CharacterStatus>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): CharacterStatus {
        return when (p?.valueAsString) {
            "looking" -> CharacterStatus.Looking
            "busy" -> CharacterStatus.Busy
            "away" -> CharacterStatus.Away
            "idle" -> CharacterStatus.Idle
            "dnd" -> CharacterStatus.DND
            "online" -> CharacterStatus.Online
            else -> CharacterStatus.Online
        }
    }
}

class CharacterStatusSerializer : JsonSerializer<CharacterStatus>() {
    override fun serialize(value: CharacterStatus?, gen: JsonGenerator?, serializers: SerializerProvider?) {
        gen?.writeString(value?.name?.toLowerCase())
    }
}
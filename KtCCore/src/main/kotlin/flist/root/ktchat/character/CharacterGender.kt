package flist.root.ktchat.character

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider

enum class CharacterGender {
    None,
    Female,
    Male,
    Shemale,
    Herm,
    Transgender,
    MaleHerm,
    CuntBoy;

    companion object {
        fun fromString(str: String): CharacterGender {
            return when (str) {
                "None" -> None
                "Female" -> Female
                "Male" -> Male
                "Shemale" -> Shemale
                "Herm" -> Herm
                "Transgender" -> Transgender
                "Male-Herm" -> MaleHerm
                "Cunt-boy" -> CuntBoy
                else -> None
            }
        }

        fun toString(characterGender: CharacterGender): String = when(characterGender) {
            None -> "None"
            Female -> "Female"
            Male -> "Male"
            Shemale -> "Shemale"
            Herm -> "Herm"
            Transgender -> "Transgender"
            MaleHerm -> "Male-Herm"
            CuntBoy -> "Cunt-boy"
        }
    }
}

class CharacterGenderDeserializer : JsonDeserializer<CharacterGender>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): CharacterGender {
        return CharacterGender.fromString(p!!.valueAsString)
    }
}

class CharacterGenderSerializer : JsonSerializer<CharacterGender>() {
    override fun serialize(value: CharacterGender?, gen: JsonGenerator?, serializers: SerializerProvider?) {
        gen?.writeString(value?.let { CharacterGender.toString(it) })
    }
}
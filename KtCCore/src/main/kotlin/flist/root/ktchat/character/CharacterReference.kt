package flist.root.ktchat.character

import flist.root.ktchat.channel.ChannelContext
import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.chat.IgnoreAction
import flist.root.ktchat.flux.propertyFromType
import flist.root.ktchat.internal.packets.client.ChannelInviteRequest
import flist.root.ktchat.internal.packets.client.IgnoreRequest
import flist.root.ktchat.internal.packets.client.PrivateMessageRequest
import flist.root.ktchat.internal.packets.server.UserStatusChange
import flist.root.ktchat.internal.packets.server.UserTypingStatus
import flist.root.ktchat.internal.packets.server.forCharacter
import reactor.core.Disposable

class CharacterReference(
        private val context: ChatContext,
        val name : String,
        val gender : CharacterGender,
        initialStatus: CharacterStatus,
        messageStr: String = "",
        ignored: Boolean = false,
        bookmarked: Boolean = false,
        friend: Boolean = false,
        var id: Int = -1
) : Disposable, Comparable<CharacterReference> {
    override fun hashCode(): Int {
        return name.hashCode()
    }

    override fun compareTo(other: CharacterReference): Int {
        return name.compareTo(other.name, true)
    }

    override fun equals(other: Any?): Boolean {
        return when(other) {
            is String -> name.compareTo(other, true) == 0
            is CharacterReference -> this.compareTo(other) == 0
            else -> false
        }
    }

    private val packets = context.packets.forCharacter(name)

    val statusChanges = packets.propertyFromType(UserStatusChange::status, initialStatus)
    val status by statusChanges

    val statusMessageChanges  = packets.propertyFromType(UserStatusChange::statusmsg, messageStr)
    val statusMessage by statusMessageChanges

    val typing = packets.propertyFromType(UserTypingStatus::status, TypingStatus.Clear)



    var ignored = ignored
        internal set

    var bookmarked = bookmarked
        internal set

    var friend = friend
        internal set

    val isOperator: Boolean
        get() = context.isOperator()

    fun invite(channelContext: ChannelContext) {
        context.send(ChannelInviteRequest(channelContext.identifier, name))
    }

    fun privateMessage(message: String) {
        context.send(PrivateMessageRequest(name, message))
    }

    fun ignore() {
        if(ignored)
            context.send(IgnoreRequest(IgnoreAction.Remove, name))
        else
            context.send(IgnoreRequest(IgnoreAction.Add, name))
    }

    override fun dispose() {
        statusChanges.dispose()
        statusMessageChanges.dispose()
        typing.dispose()
    }

    companion object {
        val Comparator : Comparator<CharacterReference> = Comparator { o1, o2 ->  String.CASE_INSENSITIVE_ORDER.compare(o1.name, o2.name)}
    }
}

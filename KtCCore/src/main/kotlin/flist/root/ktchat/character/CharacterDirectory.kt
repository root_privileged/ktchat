package flist.root.ktchat.character

import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.chat.DefaultContextAware
import flist.root.ktchat.chat.RTBType
import flist.root.ktchat.event.*
import flist.root.ktchat.internal.packets.server.*
import mu.KotlinLogging
import reactor.core.Disposable
import java.util.*

private val logger = KotlinLogging.logger {}

class CharacterDirectory(
        override val context: ChatContext,
        override val packets: PacketFlux = context.packets,
        override val events: EventFlux = context.events,
        private val characters: MutableMap<String, CharacterReference> = TreeMap(String.CASE_INSENSITIVE_ORDER)
) : Map<String, CharacterReference> by characters, DefaultContextAware {

    val operators: MutableList<String> = LinkedList()
    val bookmarks: MutableSet<String> = TreeSet(String.CASE_INSENSITIVE_ORDER)
    val ignores: MutableSet<String> = TreeSet(String.CASE_INSENSITIVE_ORDER)

    // Friends list comes from login packet. TODO.
    private val friends : MutableList<String> = LinkedList()

    override var disposables : MutableList<Disposable> = onInitialization()
    override fun onInitialization(): MutableList<Disposable> {
        return mutableListOf(
                packets onType PacketHandler()::uptimePacket,
                packets onType PacketHandler()::userOffline,
                packets onType PacketHandler()::userConnected,
                packets onType PacketHandler()::opPromotion,
                packets onType PacketHandler()::opDemotion,
                packets onType PacketHandler()::operatorList,
                packets onType PacketHandler()::characterList,
                packets onType PacketHandler()::friendList,
                packets onType PacketHandler()::ignoreList,
                packets onType PacketHandler()::realtimeBridge
        )
    }

    override fun onClientConnect() {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onClientDisconnect() {
        characters.values.onEach { it.dispose() }
        characters.clear()
    }

    override fun onClientError() {
        characters.values.onEach { it.dispose() }
        characters.clear()
    }

    fun setOfCharacters(rawCharacters: List<String>) : TreeSet<CharacterReference> {
        val rv = TreeSet(CharacterReference.Comparator)
        val intersected = keys.intersect(rawCharacters.toSet())

        rv.addAll(intersected.map(context::getCharacter))
        return rv
    }

    fun withCharacter(name: String, applyTo: CharacterReference.() -> Unit) {
        this[name]?.apply(applyTo)
    }

    private inner class PacketHandler {
        @Suppress("UNUSED_PARAMETER")
        fun uptimePacket(packet: UptimeNotification) {
            logger.info { "Received ${characters.size} characters from Server."}
        }

        fun opPromotion(packet: ChatOpPromotion) {
            operators.add(packet.character)
        }
        fun opDemotion(packet : ChatOpDemotion) {
            operators.remove(packet.character)
        }
        fun operatorList(packet: OperatorList) {
            logger.info { "Initial Operator List with ${packet.operators.count()}." }
            operators.addAll(packet.operators)
        }

        fun realtimeBridge(packet: RealTimeBridge) {
            val chr = packet.character

            val evt = when(packet.type) {
                RTBType.FriendAdd -> {
                    friends.add(chr)
                    withCharacter(chr) {
                        friend = true
                    }
                    OnFriendAdd(context, chr)
                }
                RTBType.FriendRemove -> {
                    friends.remove(chr)
                    withCharacter(chr) {
                        friend = false
                    }
                    OnFriendRemove(context, chr)
                }
                RTBType.FriendRequest -> OnFriendRequest(context, chr)
                RTBType.AddBookmark -> {
                    bookmarks.add(chr)
                    withCharacter(chr) {
                        bookmarked = true
                    }
                    OnBookmarkAdd(context, chr)
                }
                RTBType.RemoveBookmark -> {
                    bookmarks.remove(chr)
                    withCharacter(chr) {
                        bookmarked = false
                    }
                    OnBookmarkRemove(context, chr)
                }
                RTBType.Unknown -> {
                    logger.error { "Received Unknown RTB Event: ${packet.type} for character ${packet.character}"}
                    OnRTB(context, chr)
                }
            }

            logger.debug { "Received RTB Event $evt" }
            context.triggerEvent(evt)
        }

        fun ignoreList(packet: Ignore) {
            logger.info { "Ignore Packet Received ${packet.action}." }

            when(packet.action) {
                Ignore.Init -> {
                    ignores.addAll(packet.characters)
                }
                Ignore.Add -> {
                    ignores.add(packet.character)
                    withCharacter(packet.character) {
                        ignored = true
                    }
                }
                Ignore.Delete -> {
                    ignores.remove(packet.character)
                    withCharacter(packet.character) {
                        ignored = false
                    }
                }
            }
        }

        fun characterList(packet : OnlineCharacterList) {
            logger.trace { "Initial character list with ${packet.characters.count()} characters." }

            packet.characters.onEach {
                val name = it[0]
                val isIgnored = ignores.contains(name)

                val newCharacter = CharacterReference(
                        context,
                        name,
                        CharacterGender.fromString(it[1]),
                        CharacterStatus.fromString(it[2]),
                        it[3],
                        isIgnored
                )
                characters[it[0]] = newCharacter
            }
        }

        fun friendList(packet : InitialFriendsList) {
            logger.info { "Friend List Packet Received with ${packet.characters.size} characters." }
            bookmarks.addAll(packet.characters)
        }

        fun userOffline(packet: CharacterOffline) {
            withCharacter(packet.character) {
                context.triggerEvent(OnCharacterLogout(context, this))
                dispose()
            }

            logger.trace { "User Went Offline ${packet.character}" }
            characters.remove(packet.character)
        }

        fun userConnected(packet : UserConnected) {
            logger.trace { "User Came Online ${packet.identity}" }
            val isIgnored = ignores.contains(packet.identity)
            val isBookmarked = bookmarks.contains(packet.identity)
            val isFriend = friends.contains(packet.identity)
            val newCharacter = CharacterReference(
                    context,
                    packet.identity, packet.gender, packet.status,
                    ignored = isIgnored, bookmarked = isBookmarked, friend = isFriend
            )
            characters[packet.identity] = newCharacter
            context.triggerEvent(OnCharacterLogin(context, newCharacter))
        }
    }
}
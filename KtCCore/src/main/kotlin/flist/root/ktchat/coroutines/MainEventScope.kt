package flist.root.ktchat.coroutines

import flist.root.ktchat.di.KtCModules.Scopes.GlobalEventScope
import kotlinx.coroutines.CoroutineScope
import org.koin.core.context.GlobalContext
import kotlin.coroutines.CoroutineContext

class MainEventScope : CoroutineScope {
    override val coroutineContext: CoroutineContext by GlobalContext.get().koin.inject(GlobalEventScope)
}

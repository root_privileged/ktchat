package flist.root.ktchat.coroutines

import kotlinx.coroutines.CoroutineScope
import kotlin.coroutines.CoroutineContext

interface CoroutineEventScope : CoroutineScope {
    override val coroutineContext: CoroutineContext
}

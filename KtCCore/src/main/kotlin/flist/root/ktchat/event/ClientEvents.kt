package flist.root.ktchat.event

import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.chat.ConnectionStatus
import flist.root.ktchat.internal.packets.server.RawPacket

abstract class AbstractClientEvent(chatContext: ChatContext) : AbstractEvent(chatContext)

@Suppress("UNUSED_PARAMETER")
class OnRawPacket(chatContext: ChatContext, packet: RawPacket) : AbstractEvent(chatContext)


abstract class ConnectionEvent(chatContext: ChatContext) : AbstractClientEvent(chatContext) {
    fun asStatus(): ConnectionStatus = when(this) {
        is OnClientConnected -> ConnectionStatus.Connected
        is OnClientDisconnected -> ConnectionStatus.Disconnected
        is OnClientError -> ConnectionStatus.Error
        else -> ConnectionStatus.Error
    }
}

class OnClientConnected(chatContext: ChatContext) : ConnectionEvent(chatContext)
class OnClientDisconnected(chatContext: ChatContext, val reason: String) : ConnectionEvent(chatContext)
class OnClientError(chatContext: ChatContext, val reason: String) : ConnectionEvent(chatContext)

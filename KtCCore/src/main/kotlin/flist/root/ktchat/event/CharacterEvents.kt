package flist.root.ktchat.event

import flist.root.ktchat.character.CharacterReference
import flist.root.ktchat.chat.ChatContext


abstract class AbstractCharacterEvent(context: ChatContext, val character: CharacterReference) : AbstractEvent(context)
class OnCharacterLogin(context: ChatContext, character: CharacterReference) : AbstractCharacterEvent(context, character)
class OnCharacterLogout(context: ChatContext, character: CharacterReference) : AbstractCharacterEvent(context, character)
class OnPrivateMessage(context: ChatContext, character: CharacterReference, val message: String) : AbstractCharacterEvent(context, character)


abstract class AbstractRTBEvent(context: ChatContext, val forCharacter: String): AbstractEvent(context)
class OnRTB(context: ChatContext, character: String) : AbstractRTBEvent(context, character)
class OnFriendRequest(context: ChatContext, character: String) : AbstractRTBEvent(context, character)
class OnFriendAdd(context: ChatContext, character: String) : AbstractRTBEvent(context, character)
class OnFriendRemove(context: ChatContext, character: String) : AbstractRTBEvent(context, character)
class OnBookmarkAdd(context: ChatContext, character: String) : AbstractRTBEvent(context, character)
class OnBookmarkRemove(context: ChatContext, character: String) : AbstractRTBEvent(context, character)
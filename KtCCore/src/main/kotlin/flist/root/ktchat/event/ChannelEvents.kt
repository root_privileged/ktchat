package flist.root.ktchat.event

import flist.root.ktchat.channel.ChannelContext
import flist.root.ktchat.character.CharacterReference
import flist.root.ktchat.chat.ChatContext
import reactor.core.publisher.Flux
import reactor.core.publisher.ofType

typealias ChannelEvents = Flux<AbstractChannelEvent>
fun EventFlux.forChannel(channel: ChannelContext) : ChannelEvents {
    return ofType<AbstractChannelEvent>().filter {
        it.channel.identifier == channel.identifier
    }
}

abstract class AbstractChannelEvent(
        context: ChatContext,
        val channel: ChannelContext,
        val character: String,
        val characterReference: CharacterReference? = null) : AbstractEvent(context) {
    fun whenCharacter(chr: CharacterReference, block: AbstractChannelEvent.() -> Unit) {
        if(chr.name == character) {
            apply(block)
        }
    }

    fun validCharacter(block: CharacterReference.() -> Unit) {
        characterReference?.apply(block)
    }
}

class OnChannelJoined(
        context: ChatContext,
        channel: ChannelContext,
        character: CharacterReference) : AbstractChannelEvent(context, channel, character.name, character)

class OnChannelLeave(
        context: ChatContext,
        channel: ChannelContext,
        character: CharacterReference) : AbstractChannelEvent(context, channel, character.name, character)

class OnChannelMessage(
        context: ChatContext,
        channel: ChannelContext,
        character: CharacterReference,
        val message: String) : AbstractChannelEvent(context, channel, character.name, character)

class OnChannelOperatorPromotion(
        context: ChatContext,
        channel: ChannelContext,
        character: String) : AbstractChannelEvent(context, channel, character)

class OnChannelOperatorDemotion(
        context: ChatContext,
        channel: ChannelContext,
        character: String) : AbstractChannelEvent(context, channel, character)

class OnChannelOwnerChange(
        context: ChatContext,
        channel: ChannelContext,
        character: String) : AbstractChannelEvent(context, channel, character)

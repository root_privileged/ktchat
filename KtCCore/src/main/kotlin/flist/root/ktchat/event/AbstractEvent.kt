package flist.root.ktchat.event

import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.flux.Handler
import reactor.core.Disposable
import reactor.core.publisher.DirectProcessor
import reactor.core.publisher.Flux
import java.time.Instant

typealias EventEmitter = DirectProcessor<AbstractEvent>
typealias EventFlux = Flux<AbstractEvent>

abstract class AbstractEvent(val context: ChatContext) {
    val happened: Instant = Instant.now()
}

inline infix fun <reified T: AbstractEvent> Flux<T>.onEvent(noinline handler: Handler<T>): Disposable {
    return subscribe(handler)
}
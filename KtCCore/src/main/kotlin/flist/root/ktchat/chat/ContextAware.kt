package flist.root.ktchat.chat

import flist.root.ktchat.event.AbstractEvent
import flist.root.ktchat.internal.packets.server.AbstractServerPacket
import reactor.core.Disposable
import reactor.core.publisher.Flux

typealias DefaultContextAware = ContextAware<AbstractEvent, AbstractServerPacket>
interface ContextAware<E: AbstractEvent, P: AbstractServerPacket> : Disposable {
    val context: ChatContext
    val events: Flux<E>
    val packets: Flux<P>

    var disposables: MutableList<Disposable>

    override fun isDisposed(): Boolean {
        return disposables.isEmpty()
    }

    override fun dispose() {
        disposables.onEach(Disposable::dispose)
        disposables = mutableListOf()
    }

    fun onInitialization() : MutableList<Disposable>
    fun onClientConnect()
    fun onClientDisconnect()
    fun onClientError()
}
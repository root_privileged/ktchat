package flist.root.ktchat.chat

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider

enum class IgnoreAction {
    List,
    Add,
    Remove,
    Notify,
    Error
}

class IgnoreActionDeserializer : JsonDeserializer<IgnoreAction>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): IgnoreAction {
        return when (p?.valueAsString) {
            "list" -> IgnoreAction.List
            "add" -> IgnoreAction.Add
            "remove" -> IgnoreAction.Remove
            "notify" -> IgnoreAction.Notify
            else -> IgnoreAction.Error
        }
    }
}

class IgnoreActionSerializer : JsonSerializer<IgnoreAction>() {
    override fun serialize(value: IgnoreAction?, gen: JsonGenerator?, serializers: SerializerProvider?) {
        gen?.writeString(value?.name?.toLowerCase())
    }
}

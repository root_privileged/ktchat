package flist.root.ktchat.chat

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

enum class RTBType {
        AddBookmark, // trackadd
        RemoveBookmark, // trackrem
        FriendRemove, // friendremove
        FriendAdd, // friendadd
        FriendRequest, // friendrequest
        Unknown
}

class RTBTypeDeserializer : JsonDeserializer<RTBType>() {
        override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): RTBType {
                return when (p?.valueAsString) {
                        "trackadd" -> RTBType.AddBookmark
                        "trackrem" -> RTBType.RemoveBookmark
                        "friendremove" -> RTBType.FriendRemove
                        "friendadd" -> RTBType.FriendAdd
                        "friendrequest" -> RTBType.FriendRequest
                        else -> RTBType.Unknown
                }
        }
}
package flist.root.ktchat.chat

import com.tinder.scarlet.LifecycleState
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.websocket.WebSocketEvent
import flist.root.ktchat.channel.ChannelContext
import flist.root.ktchat.channel.ChannelDirectory
import flist.root.ktchat.character.CharacterDirectory
import flist.root.ktchat.character.CharacterGender
import flist.root.ktchat.character.CharacterReference
import flist.root.ktchat.character.CharacterStatus
import flist.root.ktchat.coroutines.CoroutineEventScope
import flist.root.ktchat.di.KtCModules
import flist.root.ktchat.di.KtCModules.Scopes.ChatEventScope
import flist.root.ktchat.di.ScopedKoinComponent
import flist.root.ktchat.event.*
import flist.root.ktchat.flux.FluxProperty
import flist.root.ktchat.internal.connection.ChatSocketLifecycle
import flist.root.ktchat.internal.connection.FChatSocketService
import flist.root.ktchat.internal.packets.client.AbstractClientPacket
import flist.root.ktchat.internal.packets.client.IdentificationRequest
import flist.root.ktchat.internal.packets.client.PingRequest
import flist.root.ktchat.internal.packets.client.UptimeRequest
import flist.root.ktchat.internal.packets.server.*
import flist.root.ktchat.site.AccountContext
import io.ktor.client.HttpClient
import mu.KotlinLogging
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope
import reactor.core.Disposable
import reactor.core.publisher.ofType
import reactor.core.scheduler.Scheduler
import kotlin.coroutines.CoroutineContext

private val logger = KotlinLogging.logger {}

class ChatContext(
        parent: Scope,
        val character: String
) : ScopedKoinComponent, DefaultContextAware, CoroutineEventScope {
    override val context: ChatContext = this

    override val scope: Scope = parent.getKoin().createScope("chatContext-$character", named("chat"))
    override val coroutineContext: CoroutineContext by scope.inject(ChatEventScope)

    var identified = false
        private set

    init {
        scope.declare(this)
    }


    val accountContext: AccountContext by parent.inject()
    val httpClient: HttpClient by parent.inject()

    private val scarlet: Scarlet by scope.inject()
    val socketLifecycle: ChatSocketLifecycle by scope.inject()
    val socketService: FChatSocketService = scarlet.create()


    private val eventScheduler: Scheduler by scope.inject(KtCModules.Scopes.GlobalEventScheduler)
    private val packetEmitter: PacketEmitter by scope.inject(KtCModules.Scopes.PacketEmit)
    private val eventEmitter: EventEmitter by scope.inject(KtCModules.Scopes.EventEmit)

    val characters: CharacterDirectory by scope.inject()
    val channels: ChannelDirectory by scope.inject()

    private val awareElements: List<ContextAware<*, *>> by lazy(LazyThreadSafetyMode.NONE) { listOf<ContextAware<*,*>>(
            this,
            channels,
            characters
    ) }

    override val packets : PacketFlux = packetEmitter//.publishOn(eventScheduler) avoid deadlock by publishing all packets directly from source.
    override val events: EventFlux = eventEmitter.publishOn(eventScheduler)

    val serverVariables = HashMap<String, Any>()

    val connectionStatus = FluxProperty<ConnectionStatus>(
            events.ofType<ConnectionEvent>().map(ConnectionEvent::asStatus),
            ConnectionStatus.Disconnected)


    val defaultCharacter = CharacterReference(this, "?", CharacterGender.None, CharacterStatus.Online, "Something went wrong.")


    fun isOperator() : Boolean {
        return characters.operators.contains(character)
    }

    fun isOperator(channelContext: ChannelContext) : Boolean {
        return isOperator() || channelContext.operators.contains(character)
    }

    private var socketDisposables: MutableList<Disposable> = mutableListOf(
            socketService.incomingPackets().subscribe(SocketHandler()::onPacket),
            socketService.handleSocketEvent().subscribe(SocketHandler()::onSocketEvent)
    )

    private fun resetSocket() {
        if(socketDisposables.isNotEmpty()) {
            socketDisposables.onEach(Disposable::dispose)
            socketDisposables.clear()
        }

        socketDisposables =  mutableListOf(
                socketService.incomingPackets().subscribe(SocketHandler()::onPacket),
                socketService.handleSocketEvent().subscribe(SocketHandler()::onSocketEvent)
        )
    }


    override var disposables : MutableList<Disposable> = onInitialization()
    override fun onInitialization(): MutableList<Disposable> {
        return mutableListOf(
                characters,
                channels,

                packets onType PacketHandler()::uptimePacket,
                packets onType PacketHandler()::errorPacket,
                packets onType PacketHandler()::helloPacket,
                packets onType PacketHandler()::parseErrorPacket,
                packets onType PacketHandler()::ping,
                packets onType PacketHandler()::serverVariable,
                packets onType PacketHandler()::identification,
                packets onType PacketHandler()::privateMessage
        )
    }

    override fun onClientConnect() {
        //resetSocket()
    }

    override fun onClientDisconnect() {
        serverVariables.clear()
        resetSocket()
        identified = false
    }

    override fun onClientError() {
        dispose()

        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    internal infix fun triggerEvent(event: AbstractEvent) {
        eventEmitter.onNext(event)
    }

    infix fun send(packet: AbstractClientPacket) {
        socketService.sendPacket(packet)
    }

    val logins = events.ofType<OnCharacterLogin>()
    val logouts = events.ofType<OnCharacterLogout>()
    val clientEvents = events.ofType<AbstractClientEvent>()
    val rtbEvents = events.ofType<AbstractRTBEvent>()

    val privateMessages = events.ofType<OnPrivateMessage>()


    private inner class SocketHandler {
        fun onPacket(packet: AbstractServerPacket) = packetEmitter.onNext(packet)

        fun onSocketEvent(event: WebSocketEvent) {
            when(event) {
                is WebSocketEvent.OnConnectionOpened -> {
                    triggerEvent(OnClientConnected(this@ChatContext))
                    awareElements.onEach{ it.onClientConnect() }
                    send(IdentificationRequest(accountContext.account, accountContext.ticket, character))
                    logger.info { "Chat Context $character connected and sent identification request." }
                }
                is WebSocketEvent.OnConnectionFailed -> {

                    if(socketLifecycle.currentTarget == LifecycleState.Stopped) {
                        triggerEvent(OnClientDisconnected(this@ChatContext, "Disconnected Willingly."))
                        awareElements.onEach{ it.onClientDisconnect() }
                        logger.info { "Chat Context $character disconnected because purposeful disconnection: $event" }
                    }
                    else
                    {
                        triggerEvent(OnClientError(this@ChatContext, event.toString()))
                        awareElements.onEach{ it.onClientError() }
                        logger.error { "Chat Context $character disconnected because closed channel reason: $event" }
                        socketLifecycle.stopConnection()
                    }
                }
                is WebSocketEvent.OnConnectionClosed -> {
                    triggerEvent(OnClientDisconnected(this@ChatContext, event.shutdownReason.reason))
                    awareElements.onEach{ it.onClientDisconnect() }
                    logger.info { "Chat Context $character disconnected because closed channel reason: $event" }
                }
                is WebSocketEvent.OnConnectionClosing -> {
                    triggerEvent(OnClientDisconnected(this@ChatContext, event.shutdownReason.reason))
                    awareElements.onEach{ it.onClientDisconnect() }
                    logger.info { "Chat Context $character disconnected because closed channel reason: $event" }
                }
                is WebSocketEvent.OnMessageReceived -> {

                }
            }
        }
    }

    private inner class PacketHandler {
        fun parseErrorPacket(packet: RawPacket) {
            logger.error { "Received Raw Packet $packet" }
        }

        fun errorPacket(packet: Error) {
            logger.info { "Server Error: $packet" }
        }

        fun helloPacket(packet: ServerHello) {
            logger.info { "Server says ${packet.message}" }
        }

        fun uptimePacket(packet: UptimeNotification) {
            logger.info { "Server Uptime Message: $packet"}
        }

        fun serverVariable(packet: ServerVariable) {
            logger.info{ "Server Variable ${packet.variable} received." }
            serverVariables[packet.variable] = packet.unknownProperties["value"]!!
        }

        fun privateMessage(packet: PrivateMessage) {
            triggerEvent(OnPrivateMessage(this@ChatContext, getCharacter(packet.character), packet.message))
        }

        fun identification(packet : IdentificationResponse) {
            if(character != packet.character)
                logger.error { "Somehow Identification({$packet.character}) Response isn't our character?" }
            else
            {
                identified = true
                logger.info { "Client for $character identified successfully." }
                send(UptimeRequest())
            }
        }

        @Suppress("UNUSED_PARAMETER")
        fun ping(packet: Ping) {
            logger.trace("Pong.")
            send(PingRequest())
        }
    }

    fun getCharacter(name: String = character) : CharacterReference = characters[name] ?: defaultCharacter

    internal fun isMyCharacter(characterReference: CharacterReference): Boolean {
        return characterReference.name == character
    }
    internal fun isForMyCharacter(event: AbstractChannelEvent) : Boolean {
        return event.character == character
    }
}

package flist.root.ktchat.chat

@Suppress("UNUSED_PARAMETER")
enum class ConnectionStatus(message: String = "") {
    Disconnected("Not Connected"),
    Connected("Connected"),
    Error
}
package flist.root.ktchat.chat

import com.github.h0tk3y.betterParse.combinators.AndCombinator
import com.github.h0tk3y.betterParse.combinators.times
import com.github.h0tk3y.betterParse.combinators.unaryMinus
import com.github.h0tk3y.betterParse.combinators.use
import com.github.h0tk3y.betterParse.grammar.Grammar
import com.github.h0tk3y.betterParse.grammar.parser
import com.github.h0tk3y.betterParse.lexer.Token
import com.github.h0tk3y.betterParse.lexer.TokenMatch
import com.github.h0tk3y.betterParse.parser.Parser
import com.github.h0tk3y.betterParse.utils.Tuple2


sealed class BBCodeExpression

open class ExpressionString(val string: String) : BBCodeExpression()
interface NoInnerExpressions

data class IconExpression(val characterName: String) : BBCodeExpression(), NoInnerExpressions
data class EIconExpression(val eiconName: String) : BBCodeExpression(), NoInnerExpressions
data class UserExpression(val user: String) : BBCodeExpression(), NoInnerExpressions
data class NoParseExpression(val string: String) : BBCodeExpression(), NoInnerExpressions
data class URLExpression(val url: String, val text: String) : BBCodeExpression(), NoInnerExpressions

class ColorString(string: String, val color: String) : ExpressionString(string) {

}

class ItalicString(string: String) : ExpressionString(string) {

}

class BoldString(string: String) : ExpressionString(string) {

}

data class UnderlineString(val string: String) : BBCodeExpression()

fun <T> Grammar<T>.literal(string: String): Token {
    return token(Regex.fromLiteral(string))
}

object ChatGrammar : Grammar<BBCodeExpression>() {
    override val rootParser: Parser<BBCodeExpression>
        get() = iconExpression

    val eq by literal("=")
    val urlToken by token("(?i)\\b((?:https?:(?:/{1,3}|[a-z0-9%])|[a-z0-9.\\-]+[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)/)(?:[^\\s()<>{}\\[\\]]+|\\([^\\s()]*?\\([^\\s()]+\\)[^\\s()]*?\\)|\\([^\\s]+?\\))+(?:\\([^\\s()]*?\\([^\\s()]+\\)[^\\s()]*?\\)|\\([^\\s]+?\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’])|(?:(?<!@)[a-z0-9]+(?:[.\\-][a-z0-9]+)*[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)\\b/?(?!@)))\n")
    val CLBracket by literal("[/")
    val LBracket by literal("[")
    val RBracket by literal("]")

    private val eicon by literal("eicon")
    private val user by literal("user")
    private val icon by literal("icon")
    private val noParse by literal("noparse")

    val url by literal("url")
    val color by literal("color")

    val bold by literal("b")
    val italic by literal("i")
    val strikeThrough by literal("s")
    val textSup by literal("sup")
    val textSub by literal("sub")

    private fun parseTagSimple(name: Token): AndCombinator<TokenMatch> {
        return (-LBracket * -name * -RBracket) * parser(this::words) * (-CLBracket * -name * -RBracket)
    }

    private fun parseArgumentsTag(name: Token, arg: Token): AndCombinator<Tuple2<TokenMatch, TokenMatch>> {
        return (-LBracket * -name * -eq * arg * -RBracket) * parser(this::words) * (-CLBracket * -name * -RBracket)
    }

    val iconExpression by parseTagSimple(icon) use { IconExpression(text) }
    val eiconExpression by parseTagSimple(eicon) use { EIconExpression(text) }
    val userExpression by parseTagSimple(user) use { UserExpression(text) }
    val noparseExpression by parseTagSimple(noParse) use { NoParseExpression(text) }


    val words by token("[A-Za-z0-9 ]+")
    val sentence by token("[\\s\\S]+")
    val sentenceExpression: Parser<ExpressionString> by sentence use { ExpressionString(text) }
}
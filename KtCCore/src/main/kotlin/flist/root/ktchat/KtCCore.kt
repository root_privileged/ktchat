package flist.root.ktchat

import com.google.common.io.Resources
import flist.root.ktchat.character.CharacterReference
import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.di.KtCModules
import flist.root.ktchat.event.onEvent
import flist.root.ktchat.internal.action.AbstractResponderAction
import flist.root.ktchat.internal.packets.AbstractPacket
import flist.root.ktchat.site.AccountContext
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mu.KotlinLogging
import org.koin.core.context.GlobalContext
import reactor.core.publisher.Hooks
import java.util.*
import java.util.concurrent.CountDownLatch

private val logger = KotlinLogging.logger {}

fun getLoginProperties() : Properties {
    val prop = Properties()

    val res = Resources.getResource("login.properties").openStream()

    prop.load(res)

    return prop
}

fun main() {
    AbstractPacket.InitializePackets()
    KtCModules.initialize()

    logger.debug { Hooks.onOperatorDebug() }


    val cdl = CountDownLatch(1)

    val props = getLoginProperties()

    val acct: String = props["test.account"] as String
    val pw: String = props["test.password"] as String
    val character: String = props["test.character"] as String
    val owner: String = props["test.owner"] as String

    val ctx: AccountContext = AccountContext.open(acct, pw)

    val chat: ChatContext = ctx.openCharacter(character)

    chat.privateMessages onEvent {
        if(it.character.equals(owner)) {
            when(it.message) {
                "go develop" -> {
                    chat.launch {
                        chat.channels.Actions().joinPublic("Development")
                    }
                }
                "restart" -> {
                    chat.launch {
                        chat.socketLifecycle.stopConnection()
                        delay(5000)
                        logger.info { "Reconnecting..." }
                        chat.socketLifecycle.startConnection()
                    }
                }
                "shutdown" -> {
                    chat.launch {
                        chat.socketLifecycle.stopConnection()
                        chat.httpClient.close()
                        cdl.countDown()
                    }
                }
                else -> {
                    if(it.message.startsWith("searchstatus")) {
                        val searchTerm = it.message.substringAfter("searchstatus").trim()

                        val chrs = chat.characters.filter { entry ->
                            entry.value.statusMessage.contains(searchTerm)
                        }.values.toList()

                        if(chrs.isEmpty())
                            it.character.privateMessage("No status contained search term.")
                        else {
                            val msg = "Found ${chrs.size} matching characters.\n" + chrs.joinToString { characterReference -> "[user]${characterReference.name}[/user]" }

                            it.character.privateMessage(msg)
                        }
                    }
                    else {
                        it.character.privateMessage("[eicon]not my supervisor[/eicon]")
                    }
                }
            }
        }
    }

    GlobalScope.launch {
        println("Starting Connection...")
        chat.socketLifecycle.startConnection()
    }

    cdl.await()
}

// Global Koin for specific one-per-app things.
val KtC by lazy {
    AbstractResponderAction.InitializeActions()
    AbstractPacket.InitializePackets()

    GlobalContext.get().koin
}
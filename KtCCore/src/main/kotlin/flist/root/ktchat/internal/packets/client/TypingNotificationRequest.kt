package flist.root.ktchat.internal.packets.client

import com.fasterxml.jackson.annotation.JsonProperty
import flist.root.ktchat.character.TypingStatus

class TypingNotificationRequest(@JsonProperty val character: String, val typingStatus: TypingStatus) : AbstractClientPacket("TPN")
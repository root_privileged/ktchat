package flist.root.ktchat.internal.annotations

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Retention(AnnotationRetention.RUNTIME)
annotation class ActionIdentifier(val OnCommand: String)

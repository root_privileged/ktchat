package flist.root.ktchat.internal.packets.client

import com.fasterxml.jackson.annotation.JsonProperty
import flist.root.ktchat.KtC
import flist.root.ktchat.di.KtCModules

class IdentificationRequest(
        @JsonProperty val account: String,
        @JsonProperty val ticket: String,
        @JsonProperty val character: String,
        @JsonProperty val method : String = "ticket",
        @JsonProperty("cname") val clientName: String = KtC.getProperty<String>(KtCModules.Properties.Name) ?: "ClientNameError",
        @JsonProperty("cversion") val clientVersion: String = KtC.getProperty<String>(KtCModules.Properties.Version) ?: "ClientVersionError"
) : AbstractClientPacket("IDN")

class UptimeRequest : AbstractClientPacket("UPT", true)
class PingRequest : AbstractClientPacket("PIN", true)
class PublicChannelListRequest : AbstractClientPacket("CHA", true)
class PrivateChannelListRequest : AbstractClientPacket("ORS", true)

data class ProfileTagRequest(@JsonProperty val character: String) : AbstractClientPacket("PRO")
data class ListCharacterKinks(@JsonProperty val character: String) : AbstractClientPacket("KIN")
data class CreateChannelRequest(@JsonProperty val channel: String) : AbstractClientPacket("CCR")

data class Roll_BottleRequest @JvmOverloads constructor(
        @JsonProperty val channel: String,
        @JsonProperty val dice: String = "bottle"
) : AbstractClientPacket("RLL") {
    constructor(channel: String, diceCount: Int, sides: Int) : this(channel, diceCount.toString() + "d" + sides)

    constructor(channel: String, diceCount: Int, sides: Int, additional: Int) : this(channel, diceCount.toString() + "d" + sides + "+" + additional) {}
}

package flist.root.ktchat.internal

import reactor.core.Disposable

interface GroupedDisposable : Disposable {
    val disposables: List<Disposable>
    override fun dispose() = disposables.forEach(Disposable::dispose)
}
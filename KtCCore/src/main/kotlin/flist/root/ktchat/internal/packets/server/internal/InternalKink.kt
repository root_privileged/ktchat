package flist.root.ktchat.internal.packets.server.internal

import com.fasterxml.jackson.annotation.JsonProperty

data class InternalKink(
        @JsonProperty("fetish_id") var fetish_id : String,
        @JsonProperty("character") var name : String
)

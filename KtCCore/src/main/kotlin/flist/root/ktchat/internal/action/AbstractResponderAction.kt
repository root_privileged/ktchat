package flist.root.ktchat.internal.action

import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.internal.annotations.ActionIdentifier
import flist.root.ktchat.internal.packets.server.PacketFlux
import mu.KotlinLogging
import org.reflections.Reflections
import org.reflections.scanners.SubTypesScanner
import org.reflections.scanners.TypeAnnotationsScanner
import org.reflections.util.ClasspathHelper
import org.reflections.util.ConfigurationBuilder
import reactor.core.Disposable
import java.lang.reflect.InvocationTargetException
import java.util.*
import kotlin.reflect.full.findAnnotation

private val logger = KotlinLogging.logger {}

abstract class AbstractResponderAction(val context: ChatContext, val packetFlux: PacketFlux) : Disposable {
    protected abstract val disposables: List<Disposable>
    override fun dispose() = disposables.forEach(Disposable::dispose)



    companion object {
        private val reflector = Reflections(ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage("flist.root.ktchat.internal.action"))
                .setScanners(
                        SubTypesScanner(),
                        TypeAnnotationsScanner())
        )
        private val actionCommandIndex = HashMap<String, Class<out AbstractResponderAction>>()

        fun InitializeActions() {
            val acts = reflector.getSubTypesOf(AbstractResponderAction::class.java)

            acts.stream()
                    .filter { clz -> clz.isAnnotationPresent(ActionIdentifier::class.java) }
                    .forEach { clz ->
                        val aid = clz::class.findAnnotation<ActionIdentifier>()!!
                        actionCommandIndex[aid.OnCommand] = clz
                    }

            logger.info{ "Responder Actions Indexed: ${actionCommandIndex.size}"}
        }

        fun parseActionFrom(identifier: String, withinContext: ChatContext): AbstractResponderAction? {
            var aa: AbstractResponderAction? = null

            if (actionCommandIndex.containsKey(identifier)) {
                val cClass = actionCommandIndex[identifier]!!

                try {
                    aa = cClass.getDeclaredConstructor().newInstance(withinContext)
                    logger.trace { {"Created Action ${cClass.name}"} }
                } catch (e: InstantiationException) {
                    logger.catching(e)
                } catch (e: IllegalAccessException) {
                    logger.catching(e)
                } catch (e: NoSuchMethodException) {
                    logger.catching(e)
                } catch (e: InvocationTargetException) {
                    logger.catching(e)
                }

            }

            return aa
        }
    }
}

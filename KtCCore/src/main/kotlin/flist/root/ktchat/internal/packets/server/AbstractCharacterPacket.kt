package flist.root.ktchat.internal.packets.server

import com.fasterxml.jackson.annotation.JsonProperty
import flist.root.ktchat.character.CharacterStatus
import flist.root.ktchat.chat.RTBType
import flist.root.ktchat.character.TypingStatus
import flist.root.ktchat.internal.annotations.PacketIdentifier
import reactor.core.Disposable
import reactor.core.publisher.Flux
import reactor.core.publisher.ofType

abstract class AbstractCharacterPacket(@JsonProperty val character : String = "") : AbstractServerPacket()

typealias FLN = CharacterOffline
@PacketIdentifier(Identifier = "FLN", Description = "Notification that a character went offline.")
class CharacterOffline : AbstractCharacterPacket()

typealias DOP = ChatOpDemotion
@PacketIdentifier(Identifier = "DOP", Description = "Demotes the specified character from operator status.")
class ChatOpDemotion : AbstractCharacterPacket()

typealias AOP = ChatOpPromotion
@PacketIdentifier(Identifier = "AOP", Description = "Notifies that a character has been promoted to operator.")
class ChatOpPromotion : AbstractCharacterPacket()

typealias RTB = RealTimeBridge
@PacketIdentifier(Identifier = "RTB", Description = "Real-Time Bridge indicating that a user received a note or message..")
data class RealTimeBridge(var type : RTBType) : AbstractCharacterPacket()

typealias PRI = PrivateMessage
@PacketIdentifier(Identifier = "PRI", Description = "Private message received from another user.")
data class PrivateMessage(@JsonProperty var message : String) : AbstractCharacterPacket()

typealias LRP = LookingForRP
@PacketIdentifier(Identifier = "LRP", Description = "Looking for RP Message.")
data class LookingForRP(@JsonProperty var message : String) : AbstractCharacterPacket()

typealias STA = UserStatusChange
@PacketIdentifier(Identifier = "STA", Description = "User status change.")
data class UserStatusChange(@JsonProperty var status : CharacterStatus, @JsonProperty var statusmsg : String) : AbstractCharacterPacket()

typealias TPN = UserTypingStatus
@PacketIdentifier(Identifier = "TPN", Description = "User typing notification.")
data class UserTypingStatus(@JsonProperty var status : TypingStatus) : AbstractCharacterPacket()


fun Flux<AbstractServerPacket>.forCharacter(character: String) : Flux<AbstractCharacterPacket> {
    return ofType<AbstractCharacterPacket>().filter { p -> p.character == character}
}

inline fun <reified T: AbstractCharacterPacket> Flux<AbstractServerPacket>.subscribeForCharacter(character: String, noinline handler: (T) -> Unit) : Disposable {
    return ofType<T>().filter { p -> p.character == character}.subscribe(handler)
}
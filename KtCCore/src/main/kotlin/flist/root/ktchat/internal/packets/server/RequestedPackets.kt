package flist.root.ktchat.internal.packets.server

import com.fasterxml.jackson.annotation.JsonProperty
import flist.root.ktchat.internal.annotations.PacketIdentifier
import flist.root.ktchat.internal.packets.server.internal.InternalKink
import flist.root.ktchat.internal.packets.server.internal.InternalPrivateChannel
import flist.root.ktchat.internal.packets.server.internal.InternalPublicChannel

@PacketIdentifier(Identifier = "ORS", Description = "List of all open private channels.")
data class ChannelListPrivate(@JsonProperty var channels : List<InternalPrivateChannel>) : AbstractServerPacket()

@PacketIdentifier(Identifier = "CHA", Description = "List of all public channels.")
data class ChannelListPublic(@JsonProperty var channels : List<InternalPublicChannel>) : AbstractServerPacket()

@PacketIdentifier(Identifier = "FKS", Description = "Search Results.")
data class SearchResults(
        @JsonProperty("characters") var characters : List<String>,
        @JsonProperty("kinks") var kinks : List<InternalKink>,
        @JsonProperty("genders") var genders : List<String>,
        @JsonProperty("roles") var roles : List<String>,
        @JsonProperty("orientations") var orientations : List<String>,
        @JsonProperty("positions") var positions : List<String>,
        @JsonProperty("languages") var languages : List<String>
) : AbstractServerPacket()


@PacketIdentifier(Identifier = "PRD", Description = "Profile Data.")
data class ProfileData(
        @JsonProperty("type") var type : String,
        @JsonProperty("message") var message : String,
        @JsonProperty("key") var key : String,
        @JsonProperty("innerValue") var value : String
) : AbstractServerPacket()

@PacketIdentifier(Identifier = "KID", Description = "Kinks Data.")
data class KinkData(
        @JsonProperty("type") var type : String,
        @JsonProperty("message") var message : String,
        @JsonProperty("key") var key : String,
        @JsonProperty("innerValue") var value : List<Int>
) : AbstractServerPacket()
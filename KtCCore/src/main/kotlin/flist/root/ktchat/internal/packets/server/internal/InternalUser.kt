package flist.root.ktchat.internal.packets.server.internal

import com.fasterxml.jackson.annotation.JsonProperty

data class InternalUser(
        @JsonProperty("identity") var identity : String
)
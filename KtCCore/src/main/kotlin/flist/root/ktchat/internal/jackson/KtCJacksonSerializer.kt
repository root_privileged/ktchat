package flist.root.ktchat.internal.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.client.call.TypeInfo
import io.ktor.client.features.json.JsonSerializer
import io.ktor.http.ContentType
import io.ktor.http.content.OutgoingContent
import io.ktor.http.content.TextContent
import kotlinx.io.core.Input
import kotlinx.io.core.readText

class KtCJacksonSerializer(private val objectMapper: ObjectMapper) : JsonSerializer {
    override fun write(data: Any, contentType: ContentType): OutgoingContent =
            TextContent(objectMapper.writeValueAsString(data), contentType)

    override fun read(type: TypeInfo, body: Input): Any {
        return objectMapper.readValue(body.readText(), objectMapper.typeFactory.constructType(type.reifiedType))
    }
}

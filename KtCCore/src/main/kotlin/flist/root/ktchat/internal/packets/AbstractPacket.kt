package flist.root.ktchat.internal.packets

import com.fasterxml.jackson.databind.ObjectMapper
import flist.root.ktchat.KtC
import flist.root.ktchat.internal.annotations.PacketIdentifier
import flist.root.ktchat.internal.packets.server.AbstractServerPacket
import flist.root.ktchat.internal.packets.server.RawPacket
import mu.KotlinLogging
import org.reflections.Reflections
import org.reflections.scanners.SubTypesScanner
import org.reflections.scanners.TypeAnnotationsScanner
import org.reflections.util.ClasspathHelper
import org.reflections.util.ConfigurationBuilder
import java.io.IOException
import java.lang.reflect.InvocationTargetException
import java.util.*

private val logger = KotlinLogging.logger {}

abstract class AbstractPacket {
    companion object {
        private val reflector = Reflections(ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage("flist.root.ktchat.internal.packets.server"))
                .setScanners(
                        SubTypesScanner(),
                        TypeAnnotationsScanner())
        )
        val serverPacketIndex: MutableMap<String, Class<out AbstractServerPacket>> = HashMap()

        fun InitializePackets() {
            val serverPacketClasses = reflector.getSubTypesOf(AbstractServerPacket::class.java)

            serverPacketClasses.stream()
                    .filter { clz -> clz.isAnnotationPresent(PacketIdentifier::class.java) }
                    .forEach { clz ->
                        val cid = getIdentifier(clz)
                        serverPacketIndex[cid.Identifier] = clz
                    }

            logger.info { "Server Packets Cached: ${serverPacketIndex.size}" }
        }


        fun getIdentifier(cmd: Class<out AbstractPacket>): PacketIdentifier {
            val idn = cmd.getAnnotation(PacketIdentifier::class.java)

            logger.trace { "Packet Identifier Located: ${idn.Identifier}" }
            return idn
        }

        fun parsePackets(packetData: List<String>) = packetData.map { s -> parsePacket(s) }

        fun parsePacket(packetData: String): AbstractServerPacket {
            logger.entry(packetData)

            val idn = packetData.take(3)
            var data: String? = null

            if (packetData.length > 3)
                data = packetData.substring(4)

            var cmd: AbstractServerPacket = RawPacket(packetData)

            if (serverPacketIndex.containsKey(idn)) {
                val cClass = serverPacketIndex[idn]

                if (data != null) {
                    try {
                        val objectMapper: ObjectMapper by KtC.inject()
                        cmd = objectMapper.readValue(data, cClass)
                        logger.trace { "Created Packet from JSON Reflection: ${cmd.javaClass.name}" }
                    } catch (e: IOException) {
                        logger.error { "Error reading JSON: ${e.message}" }
                    }

                } else try {
                    cmd = cClass!!.getDeclaredConstructor().newInstance()
                    logger.trace { "Created Packet from Reflected Constructor: ${cmd.javaClass.name}" }
                } catch (e: InstantiationException) {
                    logger.catching(e)
                } catch (e: IllegalAccessException) {
                    logger.catching(e)
                } catch (e: NoSuchMethodException) {
                    logger.catching(e)
                } catch (e: InvocationTargetException) {
                    logger.catching(e)
                }
            }

            if (cmd is RawPacket)
                logger.trace { "Created Raw Packet: ${cmd.javaClass.name}" }

            return cmd
        }

    }
}
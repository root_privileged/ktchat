package flist.root.ktchat.internal.packets.client

import com.fasterxml.jackson.annotation.JsonProperty

class InternalKink {
    @JsonProperty("fetish_id")
    var fetish_id: String? = null
        private set
    @JsonProperty("character")
    var name: String? = null
        private set
}

// TODO: This class is gonna need a lot of work to be usable.
class SearchRequest : AbstractClientPacket("FKS") {
    @JsonProperty("characters")
    var characters: List<String>? = null

    // the big kink thing here..
    @JsonProperty("kinks")
    var kinks: List<InternalKink>? = null

    @JsonProperty("genders")
    var genders: List<String>? = null
    @JsonProperty("roles")
    var roles: List<String>? = null
    @JsonProperty("orientations")
    var orientations: List<String>? = null
    @JsonProperty("positions")
    var positions: List<String>? = null
    @JsonProperty("languages")
    var languages: List<String>? = null
}

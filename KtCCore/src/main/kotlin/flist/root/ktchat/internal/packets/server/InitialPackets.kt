@file:Suppress("UNCHECKED_CAST")

package flist.root.ktchat.internal.packets.server

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSetter
import flist.root.ktchat.internal.annotations.PacketIdentifier
import java.time.Instant


@PacketIdentifier(Identifier = "IDN", Description = "Informs the user that the character they are logged on with successfully identified.")
class IdentificationResponse : AbstractCharacterPacket()

@PacketIdentifier(Identifier = "FRL", Description = "Initial friends list.")
data class InitialFriendsList(@JsonProperty var characters : List<String>) : AbstractServerPacket()

@PacketIdentifier(Identifier = "ADL", Description = "List containing a list of the current operators.")
data class OperatorList(@JsonProperty("ops") var operators : List<String>) : AbstractServerPacket()

@PacketIdentifier(Identifier = "LIS", Description = "Sends an array of all online character names, genders, status, and status messages.")
data class OnlineCharacterList(@JsonProperty var characters : List<List<String>>) : AbstractServerPacket()

@PacketIdentifier(Identifier = "CON", Description = "Gives a count of connected users..")
data class ConnectedUsers(@JsonProperty var message : Int) : AbstractServerPacket()

@PacketIdentifier(Identifier = "IGN", Description = "Handles ignore packet.")
data class Ignore(@JsonProperty var action : String) : AbstractServerPacket() {
    companion object {
        const val Add = "add"
        const val Delete = "delete"
        const val Init = "init"
    }

    val character: String
        @JsonIgnore
        get() = when(action) {
            Add, Delete  -> unknownProperties["character"] as String
            else -> ""
        }

    val characters: List<String>
        @JsonIgnore
        get() = when(action) {
            Init -> unknownProperties["characters"] as List<String>
            else -> emptyList()
        }
}


@PacketIdentifier(Identifier = "VAR", Description = "Server variable.")
data class ServerVariable(
        @JsonProperty("variable") var variable : String//,
        //@JsonProperty("innerValue") var innerValue : Float //
) : AbstractServerPacket() {
    val floatValue: Float
        get() {
            return unknownProperties["innerValue"] as Float
        }

    val stringListValue: List<String>
        get() {
            val p = unknownProperties["innerValue"]
            return when(p) {
                is List<*> -> p as List<String>
                else -> emptyList()
            }
        }
}

@PacketIdentifier(Identifier = "UPT", Description = "Informs the client of the server's self-tracked online time, and a few other bits of information.")
data class UptimeNotification(
        @JsonProperty var time : Instant,
        @JsonProperty var startTime : Instant,
        @JsonProperty("startstring") var startString : String,
        @JsonProperty var accepted : Int,
        @JsonProperty var channels : Int,
        @JsonProperty var users : Int,
        @JsonProperty("maxusers") var maxUsers : Int
) : AbstractServerPacket() {

    @JsonSetter("time")
    private fun setTime(time: Long) {
        this.time = Instant.ofEpochSecond(time)
    }

    @JsonSetter("starttime")
    private fun setStartTime(startTime: Long) {
        this.startTime = Instant.ofEpochSecond(startTime)
    }
}
package flist.root.ktchat.internal.packets.client

import com.fasterxml.jackson.annotation.JsonProperty

data class CreateOfficialChannelRequest(@JsonProperty val channel: String) : AbstractClientPacket("CRC")

data class ListAlternatesRequest(@JsonProperty val character: String) : AbstractClientPacket("AMC")

data class BroadcastRequest(@JsonProperty val message: String) : AbstractClientPacket("BRO")

data class PromoteOperatorRequest(@JsonProperty val character: String) : AbstractClientPacket("AOP")
data class DemoteOperatorRequest(@JsonProperty val character: String) : AbstractClientPacket("DOP")

data class ServerTimeoutRequest(
        @JsonProperty val character: String,
        @JsonProperty("time") var minutes: Int,
        @JsonProperty val reason: String
) : AbstractClientPacket("TMO")

data class ServerBanRequest(@JsonProperty val character: String) : AbstractClientPacket("ACB")
data class ServerKickRequest(@JsonProperty val character: String) : AbstractClientPacket("KIK")
data class ServerUnbanRequest(@JsonProperty val character: String) : AbstractClientPacket("UBN")

data class RewardRequest(@JsonProperty val character: String) : AbstractClientPacket("RWD")

data class ReloadConfigurationRequest(@JsonProperty val save: String) : AbstractClientPacket("RLD")
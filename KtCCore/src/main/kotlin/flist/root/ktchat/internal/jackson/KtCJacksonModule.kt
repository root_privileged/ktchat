package flist.root.ktchat.internal.jackson

import com.fasterxml.jackson.databind.module.SimpleModule
import flist.root.ktchat.channel.ChannelMode
import flist.root.ktchat.channel.ChannelModeDeserializer
import flist.root.ktchat.channel.ChannelModeSerializer
import flist.root.ktchat.character.*
import flist.root.ktchat.chat.*

class KtCJacksonModule : SimpleModule() {
    init {
        addSerializer(ChannelMode::class.java, ChannelModeSerializer())
        addDeserializer(ChannelMode::class.java, ChannelModeDeserializer())

        addSerializer(TypingStatus::class.java, TypingStatusSerializer())
        addDeserializer(TypingStatus::class.java, TypingStatusDeserializer())

        addSerializer(CharacterStatus::class.java, CharacterStatusSerializer())
        addDeserializer(CharacterStatus::class.java, CharacterStatusDeserializer())

        addSerializer(CharacterGender::class.java, CharacterGenderSerializer())
        addDeserializer(CharacterGender::class.java, CharacterGenderDeserializer())

        addSerializer(IgnoreAction::class.java, IgnoreActionSerializer())
        addDeserializer(IgnoreAction::class.java, IgnoreActionDeserializer())

        addDeserializer(RTBType::class.java, RTBTypeDeserializer())
    }
}
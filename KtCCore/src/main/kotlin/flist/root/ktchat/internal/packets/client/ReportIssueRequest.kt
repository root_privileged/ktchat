package flist.root.ktchat.internal.packets.client

import com.fasterxml.jackson.annotation.JsonProperty

// TODO: Simulate webclient way of submitting reports.
//  It is suspected that third-party clients cannot upload logs.
data class ReportIssueRequest(
        @JsonProperty val action: String,
        @JsonProperty var report: String,
        @JsonProperty var character: String
) : AbstractClientPacket("SFC")

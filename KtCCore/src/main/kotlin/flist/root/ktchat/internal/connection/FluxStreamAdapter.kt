package flist.root.ktchat.internal.connection

import com.tinder.scarlet.Stream
import com.tinder.scarlet.StreamAdapter
import com.tinder.scarlet.utils.getRawType
import mu.KotlinLogging
import reactor.core.publisher.Flux
import java.lang.reflect.Type

private val logger = KotlinLogging.logger {}

class FluxStreamAdapter<T> : StreamAdapter<T, Flux<T>> {
    override fun adapt(stream: Stream<T>): Flux<T> {


        return Flux.from(stream).doOnError{ logger.error { "Error in flux stream $it" }}
    }
}

class ReactorStreamAdapterFactory : StreamAdapter.Factory {
    override fun create(type: Type): StreamAdapter<Any, Any> {
        return when(type.getRawType()) {
            Flux::class.java -> FluxStreamAdapter()
            else -> throw IllegalArgumentException("$type is not supported by this StreamAdapterFactory")
        }
    }

}
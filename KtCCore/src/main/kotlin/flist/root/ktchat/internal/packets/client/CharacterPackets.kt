package flist.root.ktchat.internal.packets.client

import com.fasterxml.jackson.annotation.JsonProperty
import flist.root.ktchat.character.CharacterStatus
import flist.root.ktchat.chat.IgnoreAction

class CharacterStatusUpdateRequest(
        @JsonProperty val status: CharacterStatus,
        @JsonProperty val statusmsg: String
) : AbstractClientPacket("STA")

class IgnoreRequest(
        @JsonProperty val action: IgnoreAction,
        @JsonProperty val character: String
) : AbstractClientPacket("IGN")

data class PrivateMessageRequest(
        @JsonProperty("recipient") val recipient: String,
        @JsonProperty("message") var message: String
) : AbstractClientPacket("PRI")

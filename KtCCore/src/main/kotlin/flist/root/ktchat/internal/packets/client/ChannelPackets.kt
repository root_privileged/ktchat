package flist.root.ktchat.internal.packets.client

import com.fasterxml.jackson.annotation.JsonProperty
import flist.root.ktchat.channel.ChannelMode

typealias CDS = ChannelDescriptionRequest
data class ChannelDescriptionRequest(
        @JsonProperty val channel: String,
        @JsonProperty var description: String) : AbstractClientPacket("CDS")

typealias CBL = ChannelBanlistRequest
data class ChannelBanlistRequest(@JsonProperty val channel: String) : AbstractClientPacket("CBL")

typealias CBU = ChannelBanRequest
data class ChannelBanRequest(
        @JsonProperty val channel: String,
        @JsonProperty val character: String
) : AbstractClientPacket("CBU")

typealias CIU = ChannelInviteRequest
data class ChannelInviteRequest(
        @JsonProperty var channel: String,
        @JsonProperty var character: String
) : AbstractClientPacket("CIU")

typealias JCH = ChannelJoinRequest
data class ChannelJoinRequest(@JsonProperty var channel: String) : AbstractClientPacket("JCH")

typealias CKU = ChannelKickRequest
data class ChannelKickRequest(
        @JsonProperty var channel: String,
        @JsonProperty var character: String
) : AbstractClientPacket("CKU")

typealias LCH = ChannelLeaveRequest
data class ChannelLeaveRequest(@JsonProperty var channel: String) : AbstractClientPacket("LCH")

typealias MSG = ChannelMessageRequest
data class ChannelMessageRequest(
        @JsonProperty val channel: String,
        @JsonProperty val message: String
) : AbstractClientPacket("MSG")

typealias RMO = ChannelModeChangeRequest
data class ChannelModeChangeRequest(
        @JsonProperty val channel: String,
        @JsonProperty val mode: ChannelMode
) : AbstractClientPacket("RMO")

typealias COR = ChannelOperatorDemotionRequest
data class ChannelOperatorDemotionRequest(
        @JsonProperty val channel: String,
        @JsonProperty val character: String
) : AbstractClientPacket("COR")

typealias COA = ChannelOperatorPromotionRequest
data class ChannelOperatorPromotionRequest(
        @JsonProperty val channel: String,
        @JsonProperty val character: String
) : AbstractClientPacket("COA")

typealias CSO = ChannelOwnerSetRequest
data class ChannelOwnerSetRequest(
        @JsonProperty val channel: String,
        @JsonProperty val character: String
) : AbstractClientPacket("CSO")

typealias RST = ChannelPrivacyChangeRequest
data class ChannelPrivacyChangeRequest(
        @JsonProperty val channel: String,
        @JsonProperty val status: String
) : AbstractClientPacket("RST")

typealias CTU = ChannelTemporaryBanRequest
data class ChannelTemporaryBanRequest(
        @JsonProperty val channel: String,
        @JsonProperty val character: String,
        @JsonProperty("length") val minutes: Int
) : AbstractClientPacket("CTU")

typealias CUB = ChannelUnbanRequest
data class ChannelUnbanRequest(
        @JsonProperty val channel: String,
        @JsonProperty val character: String
) : AbstractClientPacket("CUB")

typealias COL = ListChannelOperatorsRequest
data class ListChannelOperatorsRequest(@JsonProperty val channel: String) : AbstractClientPacket("COL")
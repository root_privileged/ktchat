@file:Suppress("UNCHECKED_CAST")

package flist.root.ktchat.internal.packets.server

import com.fasterxml.jackson.annotation.JsonProperty
import flist.root.ktchat.internal.annotations.PacketIdentifier

@PacketIdentifier(Identifier = "RLL", Description = "Dice roll packet or bottle spin packet.")
data class Roll_Bottle(
        @JsonProperty var character : String,
        @JsonProperty var message : String,
        @JsonProperty var channel : String,
        @JsonProperty var type : String
) : AbstractServerPacket() {
    val rolls: List<String>
        get() = (unknownProperties["rolls"] as List<String>)

    val endResult: Int
        get() = (unknownProperties["endresult"] as Int).toInt()

    val results: List<Int>
        get() = unknownProperties["results"] as List<Int>

    val target: String
        get() = unknownProperties["target"] as String
}


@PacketIdentifier(Identifier = "CIU", Description = "Invites a user to a title.")
data class ChannelInvite(
        @JsonProperty("sender") var sender : String,
        @JsonProperty("title") var title : String,
        @JsonProperty("character") var name : String
) : AbstractServerPacket()
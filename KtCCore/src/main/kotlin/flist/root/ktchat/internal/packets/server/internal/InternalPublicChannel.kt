package flist.root.ktchat.internal.packets.server.internal

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSetter
import flist.root.ktchat.channel.ChannelMode

data class InternalPublicChannel(
        @JsonProperty("name") var name : String,
        @JsonProperty("mode") var mode : ChannelMode,
        @JsonProperty("characters") var characters : Int
) {
    @JsonSetter("mode")
    fun setMode(modeStr: String): ChannelMode {
        return when(modeStr) {
            "both" -> ChannelMode.Both
            "chat" -> ChannelMode.Chat
            else -> ChannelMode.RP
        }
    }
}
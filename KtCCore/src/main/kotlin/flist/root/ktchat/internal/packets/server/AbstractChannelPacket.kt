package flist.root.ktchat.internal.packets.server

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import flist.root.ktchat.channel.ChannelMode
import flist.root.ktchat.internal.annotations.PacketIdentifier
import flist.root.ktchat.internal.packets.server.internal.InternalUser
import reactor.core.Disposable
import reactor.core.publisher.Flux
import reactor.core.publisher.ofType

abstract class AbstractChannelPacket(@JsonProperty val channel : String = "") : AbstractServerPacket()

typealias RMO = ChangeRoomMode
@PacketIdentifier(Identifier = "RMO", Description = "Room mode change.")
data class ChangeRoomMode(@JsonProperty("mode") var modeStr : String) : AbstractChannelPacket() {
    @JsonIgnore
    val mode: ChannelMode = when(modeStr) {
        "both" -> ChannelMode.Both
        "chat" -> ChannelMode.Chat
        else -> ChannelMode.RP
    }
}

typealias CBU = ChannelBan
@PacketIdentifier(Identifier = "CBU", Description = "Bans the specified user from the specified channel.")
data class ChannelBan(@JsonProperty var operator : String, @JsonProperty("character") var character : String) : AbstractChannelPacket()

typealias CDS = ChannelDescriptionUpdate
@PacketIdentifier(Identifier = "CDS", Description = "Notifies the client that the specified channels description has changed")
data class ChannelDescriptionUpdate(@JsonProperty var description : String) : AbstractChannelPacket()

typealias CKU = ChannelKick
@PacketIdentifier(Identifier = "CKU", Description = "Kicks the specified user from the specified channel.")
data class ChannelKick(@JsonProperty var operator : String, @JsonProperty var name : String) : AbstractChannelPacket()

typealias MSG = ChannelMessage
@PacketIdentifier(Identifier = "MSG", Description = "Channel message received from another user.")
data class ChannelMessage(@JsonProperty val character : String, @JsonProperty val message : String) : AbstractChannelPacket()

typealias COR = ChannelOpDemotion
@PacketIdentifier(Identifier = "COR", Description = "Demotes a channel operator.")
data class ChannelOpDemotion(@JsonProperty var character : String) : AbstractChannelPacket()

typealias COA = ChannelOpPromotion
@PacketIdentifier(Identifier = "COA", Description = "Promotes a user to channel operator.")
data class ChannelOpPromotion(@JsonProperty val character : String) : AbstractChannelPacket()

typealias JCH = CharacterJoinChannel
@PacketIdentifier(Identifier = "JCH", Description = "Notifies that a character has joined the specified channel, also can be your own character..")
data class CharacterJoinChannel(@JsonProperty var title : String, @JsonProperty var character : InternalUser) : AbstractChannelPacket()

typealias LCH = CharacterLeftChannel
@PacketIdentifier(Identifier = "LCH", Description = "Notifies that a character has left the specified channel, also can be your own character.")
data class CharacterLeftChannel(@JsonProperty val character : String) : AbstractChannelPacket()

typealias CSO = SetChannelOwner
@PacketIdentifier(Identifier = "CSO", Description = "Sets the owner of the specified channel to the character provided.")
data class SetChannelOwner(@JsonProperty var character : String) : AbstractChannelPacket()

typealias COL = ChannelOperatorList
@PacketIdentifier(Identifier = "COL", Description = "List of operators in the specified channel.")
data class ChannelOperatorList(@JsonProperty("oplist") val operators : List<String>) : AbstractChannelPacket() {
    // Entry 0 is always the owner/operator
    val owner: String
        @JsonIgnore
        get() = operators[0]

    val moderators: List<String>
        @JsonIgnore
        get() = if (operators.size > 1) {
            operators.subList(1, operators.size - 1)
        } else
            emptyList()
}


@PacketIdentifier(Identifier = "ICH", Description = "Initial channel database.")
data class InitialChannelData(@JsonProperty var mode : ChannelMode, @JsonProperty var users : List<InternalUser>) : AbstractChannelPacket() {
    val characters = users.map(InternalUser::identity)
}


fun Flux<AbstractServerPacket>.forChannel(channel: String) : Flux<AbstractChannelPacket> {
    return ofType<AbstractChannelPacket>().filter { p -> p.channel == channel}
}

inline fun <reified T: AbstractChannelPacket> Flux<AbstractChannelPacket>.subscribeForType(noinline handler: (T) -> Unit) : Disposable {
    return ofType<T>().subscribe(handler)
}
package flist.root.ktchat.internal

import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.internal.packets.server.PacketFlux

interface ChatContextual {
    val context: ChatContext

    val packets: PacketFlux
        get() = context.packets
}
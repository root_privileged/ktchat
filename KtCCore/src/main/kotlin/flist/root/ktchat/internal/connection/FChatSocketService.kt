package flist.root.ktchat.internal.connection

import com.tinder.scarlet.websocket.WebSocketEvent
import com.tinder.scarlet.ws.Receive
import com.tinder.scarlet.ws.Send
import flist.root.ktchat.internal.packets.client.AbstractClientPacket
import flist.root.ktchat.internal.packets.server.AbstractServerPacket
import reactor.core.publisher.Flux

interface FChatSocketService {
    @Receive
    fun handleSocketEvent() : Flux<WebSocketEvent>

    @Send
    fun sendPacket(packet: AbstractClientPacket)

    @Receive
    fun incomingPackets() : Flux<AbstractServerPacket>
}
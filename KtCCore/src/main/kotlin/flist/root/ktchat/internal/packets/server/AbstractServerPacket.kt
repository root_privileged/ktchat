package flist.root.ktchat.internal.packets.server

import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import flist.root.ktchat.character.CharacterGender
import flist.root.ktchat.character.CharacterStatus
import flist.root.ktchat.flux.Handler
import flist.root.ktchat.internal.annotations.PacketIdentifier
import flist.root.ktchat.internal.packets.AbstractPacket
import reactor.core.Disposable
import reactor.core.publisher.DirectProcessor
import reactor.core.publisher.Flux
import reactor.core.publisher.ofType
import java.time.Instant
import java.util.*

typealias PacketFlux = Flux<AbstractServerPacket>
typealias ChannelPacketFlux = Flux<AbstractChannelPacket>
typealias PacketEmitter = DirectProcessor<AbstractServerPacket>

abstract class AbstractServerPacket : AbstractPacket() {
    @JsonIgnore
    val timeReceived: Instant = Instant.now()

    @JsonAnySetter
    val unknownProperties: MutableMap<String, Any> = HashMap()
}


@PacketIdentifier(Identifier = "PIN", HeaderOnly = true, Description = "Server ping packet, requires a response to keep connection alive.")
class Ping : AbstractServerPacket() {
    override fun toString(): String {
        return "PIN: Pong"
    }
}

typealias BRO = AdminBroadcast
@PacketIdentifier(Identifier = "BRO", Description = "Admin server broadcast message.")
data class AdminBroadcast(@JsonProperty val message : String) : AbstractServerPacket()

typealias HLO = ServerHello
@PacketIdentifier(Identifier = "HLO", Description = "Server Hello packet.")
data class ServerHello(@JsonProperty var message : String) : AbstractServerPacket()

typealias SYS = SystemMessage
@PacketIdentifier(Identifier = "SYS", Description = "System message from the server, can be mixed in with other packet responses.")
data class SystemMessage(@JsonProperty val channel : String = "", @JsonProperty var message : String) : AbstractServerPacket()

typealias NLN = UserConnected
@PacketIdentifier(Identifier = "NLN", Description = "User connected.")
data class UserConnected(
        @JsonProperty var identity : String,
        @JsonProperty var gender : CharacterGender,
        @JsonProperty var status: CharacterStatus
) : AbstractServerPacket()

// Re: https://wiki.f-list.net/F-Chat_Error_Codes
typealias ERR = Error
@PacketIdentifier(Identifier = "ERR", Description = "An error has occurred.")
data class Error(@JsonProperty val number : Int, @JsonProperty val message : String) : AbstractServerPacket()



inline fun <reified T: AbstractServerPacket> PacketFlux.subscribeForType(noinline handler: Handler<T>) : Disposable {
    return ofType<T>().subscribe(handler)
}

inline infix fun <reified T: AbstractServerPacket> PacketFlux.onType(noinline handler: Handler<T>) : Disposable {
    return ofType<T>().subscribe(handler)
}

package flist.root.ktchat.internal.connection

import com.tinder.scarlet.Message
import com.tinder.scarlet.MessageAdapter
import flist.root.ktchat.internal.packets.AbstractPacket
import flist.root.ktchat.internal.packets.client.AbstractClientPacket
import flist.root.ktchat.internal.packets.server.AbstractServerPacket
import flist.root.ktchat.internal.packets.server.RawPacket
import java.lang.reflect.Type

class ServerPacketMessageAdapter : MessageAdapter<AbstractPacket> {
    override fun toMessage(data: AbstractPacket): Message {
        when (data) {
            is AbstractClientPacket -> {
                return Message.Text(data.data())
            }
            is AbstractServerPacket -> {
                // TODO: Possible for replaying test scenarios but.. Eh?
            }
        }

        return Message.Text("ERR") // TODO: Something borked.
    }

    override fun fromMessage(message: Message): AbstractServerPacket {
        return when (message) {
            is Message.Text -> AbstractPacket.parsePacket(message.value)
            is Message.Bytes -> RawPacket("") // Is this ever going to be actually called? I doubt it.
        }
    }

    class Factory : MessageAdapter.Factory {
        override fun create(type: Type, annotations: Array<Annotation>): MessageAdapter<*> {
            return ServerPacketMessageAdapter()
        }

    }
}
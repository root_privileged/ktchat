package flist.root.ktchat.internal.packets.server.internal

import com.fasterxml.jackson.annotation.JsonProperty

data class InternalPrivateChannel(
        /**
         * ADH-<Hexcode> Identifier for use in all referencing to private channels.
         */
        @JsonProperty("name") var name : String,

        /**
         * Visible title for display.
         */
        @JsonProperty("title") var title : String,
        @JsonProperty("characters") var characters : Int
)
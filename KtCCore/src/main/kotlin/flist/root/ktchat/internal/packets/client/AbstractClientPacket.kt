package flist.root.ktchat.internal.packets.client

import com.fasterxml.jackson.databind.ObjectMapper
import flist.root.ktchat.KtC
import flist.root.ktchat.internal.packets.AbstractPacket
import reactor.core.publisher.DirectProcessor

typealias PacketSink = DirectProcessor<AbstractClientPacket>

abstract class AbstractClientPacket(open val header: String, open val headerOnly: Boolean = false) : AbstractPacket() {
    fun data() : String {
        val objectMapper: ObjectMapper by KtC.inject()

        return if(headerOnly)
            header
        else
            "$header ${objectMapper.writeValueAsString(this)}"
    }
}
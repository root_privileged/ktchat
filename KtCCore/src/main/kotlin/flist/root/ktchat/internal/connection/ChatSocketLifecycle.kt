package flist.root.ktchat.internal.connection

import com.tinder.scarlet.Lifecycle
import com.tinder.scarlet.LifecycleState
import com.tinder.scarlet.lifecycle.LifecycleRegistry

class ChatSocketLifecycle(
        private val lifecycle: LifecycleRegistry = LifecycleRegistry()
) : Lifecycle by lifecycle {
    init {
        lifecycle.onNext(LifecycleState.Stopped)
    }

    var currentTarget: LifecycleState = LifecycleState.Stopped

    fun startConnection() {
        lifecycle.onNext(LifecycleState.Started)
        currentTarget = LifecycleState.Started
    }

    fun stopConnection() {
        lifecycle.onNext(LifecycleState.Stopped)
        currentTarget = LifecycleState.Stopped
    }
}
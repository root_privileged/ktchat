package flist.root.ktchat.internal.packets.server

data class RawPacket(val raw: String) : AbstractServerPacket()

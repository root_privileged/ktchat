package flist.root.ktchat.flux

import reactor.core.CoreSubscriber
import reactor.core.Disposable
import reactor.core.publisher.Flux
import java.util.function.Consumer
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class FluxProperty<T: Any>(
        src: Flux<T>,
        defaultValue: T) : Flux<T>(), Consumer<T>, Disposable, ReadOnlyProperty<Any, T> {
    override fun getValue(thisRef: Any, property: KProperty<*>): T {
        return innerValue
    }

    private val source : Flux<T> = src
    private var disposable: Disposable = source.subscribe(this)

    var innerValue : T = defaultValue
        private set

    override fun subscribe(actual: CoreSubscriber<in T>) {
        source.subscribe(actual)
    }

    override fun accept(t: T) {
        innerValue = t
    }

    override fun dispose() {
        disposable.dispose()
    }
}
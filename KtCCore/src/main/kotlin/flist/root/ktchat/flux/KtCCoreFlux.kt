package flist.root.ktchat.flux

import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.event.AbstractEvent
import flist.root.ktchat.event.EventFlux
import flist.root.ktchat.internal.packets.server.AbstractServerPacket
import reactor.core.publisher.Flux
import reactor.core.publisher.ofType

typealias Handler<T> = (T) -> Unit


inline fun <reified T: I, O, I> Flux<I>.ofTypeMapped(noinline mapper: (T) -> O): Flux<O> {
    return ofType(T::class.java).map(mapper)
}

inline fun <I, O> mapWithContext(context: ChatContext, crossinline mapper: (ChatContext, I) -> O) : (I) -> O {
    return { i -> mapper(context, i)}
}
inline fun <reified T: AbstractServerPacket, O: AbstractEvent> Flux<AbstractServerPacket>.asEventFlux(context: ChatContext, noinline mapper: (ChatContext, T) -> O) : Flux<O> {
    return this.ofType<T>().map(mapWithContext(context, mapper))
}


inline fun <reified T: I, O : Any, I> Flux<I>.propertyFromType(noinline mapper: (T) -> O, defaultValue: O): FluxProperty<O> {
    return FluxProperty(ofType(T::class.java).map(mapper), defaultValue)
}
fun <I : AbstractServerPacket, O : Any> Flux<I>.fromTypeFiltered(pred : (I) -> Boolean, mapper: (I) -> O, defaultValue: O): FluxProperty<O> {
    return FluxProperty(filter(pred).map(mapper), defaultValue)
}



inline fun <reified T: AbstractEvent> EventFlux.ofFilteredEvent(noinline predicate: (T) -> Boolean): Flux<T> {
    return ofType(T::class.java).filter(predicate)
}
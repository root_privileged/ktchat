package flist.root.ktchat.channel

import flist.root.ktchat.character.CharacterReference
import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.event.*
import flist.root.ktchat.flux.propertyFromType
import flist.root.ktchat.internal.packets.client.ChannelDescriptionRequest
import flist.root.ktchat.internal.packets.client.ChannelModeChangeRequest
import flist.root.ktchat.internal.packets.client.ChannelOperatorDemotionRequest
import flist.root.ktchat.internal.packets.client.ChannelOperatorPromotionRequest
import flist.root.ktchat.internal.packets.client.LCH
import flist.root.ktchat.internal.packets.client.MSG
import flist.root.ktchat.internal.packets.server.*
import reactor.core.Disposable
import reactor.core.publisher.ofType
import java.util.*
import kotlin.collections.ArrayList

class ChannelContext(
        val chatContext: ChatContext,
        val name: String,
        val identifier: String = name // For public channels this is identical to the character, for private channels less so.
) : Disposable {
    var initialized = false
        private set
    val packets = chatContext.packets.forChannel(identifier)

    val events: ChannelEvents = chatContext.events.forChannel(this)

    val private = name != identifier

    val description = packets.propertyFromType(ChannelDescriptionUpdate::description, "")
    val mode = packets.propertyFromType(ChangeRoomMode::mode, "")
    val operators: MutableList<String> = ArrayList()
    lateinit var characters: TreeSet<CharacterReference> //= chatContext.characters.setOfCharacters(chars)

    override fun dispose() = disposables.forEach(Disposable::dispose)
    private val disposables : List<Disposable> = listOf(
            description,
            mode,
            chatContext.packets.subscribeForType(PacketHandler()::characterOffline),
            packets.subscribeForType(InititalizationHandler()::initialChannelData),
            packets.subscribeForType(InititalizationHandler()::channelOperatorList),
            packets.subscribeForType(InititalizationHandler()::descriptionUpdate),
            packets.subscribeForType(PacketHandler()::characterLeave),
            packets.subscribeForType(PacketHandler()::characterJoin),
            packets.subscribeForType(PacketHandler()::opDemotion),
            packets.subscribeForType(PacketHandler()::opPromotion),
            packets.subscribeForType(PacketHandler()::setOwner),
            packets.subscribeForType(PacketHandler()::channelMessage)
    )

    inner class Actions {
        /**
         * Send a message to this [ChannelContext].
         */
        fun message(msg: String) {
            chatContext.send(MSG(identifier, msg))
        }

        fun leave() {
            chatContext.send(LCH(identifier))
        }

        fun updateDescription(newDescription: String) {
            // TODO: Throw error if not allowed?
            if(chatContext.isOperator(this@ChannelContext))
                chatContext.send(ChannelDescriptionRequest(identifier, newDescription))
        }

        // TODO: Mode string.
        fun setMode(newMode: ChannelMode) {
            if(chatContext.isOperator(this@ChannelContext))
                chatContext.send(ChannelModeChangeRequest(identifier, newMode))
        }

        fun addOperator(newOperator: String) {
            if(chatContext.isOperator(this@ChannelContext))
                chatContext.send(ChannelOperatorPromotionRequest(identifier, newOperator))
        }
        fun addOperator(characterReference: CharacterReference) = addOperator(characterReference.name)

        fun removeOperator(oldOperator: String) {
            if(chatContext.isOperator(this@ChannelContext))
                chatContext.send(ChannelOperatorDemotionRequest(identifier, oldOperator))
        }
        fun removeOperator(characterReference: CharacterReference) = removeOperator(characterReference.name)


    }

    inner class Events {
        val joins = events.ofType<OnChannelJoined>()
        val left = events.ofType<OnChannelLeave>()
        val messages = events.ofType<OnChannelMessage>()
    }

    /*
     * Separated handlers for the 'initialization' portion of the channel coming to be usable.
     *
     * Process is client sends JCH to target channel, receives a JCH from server starting process
     * over next few moments follows a COL (if the channel is private) and then an ICH and CDS packet.
     *
     * Return JCH is handled before the channel context is created and is what actually triggers its creation.
     */
    private inner class InititalizationHandler {
        // COL
        fun channelOperatorList(packet: ChannelOperatorList) {
            operators.addAll(packet.moderators)
        }

        // ICH
        fun initialChannelData(packet: InitialChannelData) {
            characters = chatContext.characters.setOfCharacters(packet.characters)
        }

        // CDS
        @Suppress("UNUSED_PARAMETER")
        fun descriptionUpdate(packet: ChannelDescriptionUpdate) {
            if(!initialized)
                chatContext.triggerEvent(OnChannelJoined(chatContext, this@ChannelContext, chatContext.getCharacter()))

            initialized = true
            // TOOD: Add self event or handled by below?
        }

        fun channelLeave() {

        }
    }

    private inner class PacketHandler {
        fun characterOffline(packet: CharacterOffline) {
            if(initialized) {
                val chr = chatContext.getCharacter(packet.character)
                chatContext.triggerEvent(OnChannelLeave(chatContext, this@ChannelContext, chr))
                characters.remove(chr)
            }
        }

        fun characterLeave(packet: CharacterLeftChannel) {
            if(initialized) {
                val chr = chatContext.getCharacter(packet.character)
                chatContext.triggerEvent(OnChannelLeave(chatContext, this@ChannelContext, chr))
                characters.remove(chr)
            }
        }

        fun characterJoin(packet: CharacterJoinChannel) {
            if(initialized) {
                val chr = chatContext.getCharacter(packet.character.identity)
                characters.add(chr)
                chatContext.triggerEvent(OnChannelJoined(chatContext, this@ChannelContext, chr))
            }
        }

        fun channelMessage(packet: ChannelMessage) {
            val chr = chatContext.getCharacter(packet.character)
            chatContext.triggerEvent(OnChannelMessage(chatContext, this@ChannelContext, chr, packet.message))
        }

        fun opDemotion(packet: ChannelOpDemotion) {
            if(initialized) {
                operators.remove(packet.character)
                chatContext.triggerEvent(OnChannelOperatorDemotion(chatContext, this@ChannelContext, packet.character))
            }
        }

        fun opPromotion(packet: ChannelOpPromotion){
            if(initialized) {
                operators.add(packet.character)
                chatContext.triggerEvent(OnChannelOperatorPromotion(chatContext, this@ChannelContext, packet.character))
            }
        }

        fun setOwner(packet: SetChannelOwner) {
            if(initialized) {
                operators[0] = packet.character
                chatContext.triggerEvent(OnChannelOwnerChange(chatContext, this@ChannelContext, packet.character))
            }

        }
    }
}
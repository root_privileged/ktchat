package flist.root.ktchat.channel

import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.chat.DefaultContextAware
import flist.root.ktchat.event.EventFlux
import flist.root.ktchat.event.OnChannelJoined
import flist.root.ktchat.event.OnChannelLeave
import flist.root.ktchat.event.onEvent
import flist.root.ktchat.flux.ofFilteredEvent
import flist.root.ktchat.internal.packets.client.ChannelJoinRequest
import flist.root.ktchat.internal.packets.client.PrivateChannelListRequest
import flist.root.ktchat.internal.packets.client.PublicChannelListRequest
import flist.root.ktchat.internal.packets.server.*
import flist.root.ktchat.internal.packets.server.internal.InternalPrivateChannel
import flist.root.ktchat.internal.packets.server.internal.InternalPublicChannel
import mu.KotlinLogging
import reactor.core.Disposable
import java.time.Instant
import java.util.*
import kotlin.collections.set


private val logger = KotlinLogging.logger {}

class ChannelDirectory(
        override val context: ChatContext,
        override val events: EventFlux = context.events,
        override val packets: PacketFlux = context.packets,
        private val channels: MutableList<ChannelContext> = mutableListOf()
) : List<ChannelContext> by channels, DefaultContextAware {
    private val publicChannels = TreeMap<String, PublicChannelRecord>()
    private val privateChannels = TreeMap<String, PrivateChannelRecord>()

    val channelJoin = events.ofFilteredEvent<OnChannelJoined>(context::isForMyCharacter)
    val channelLeave = events.ofFilteredEvent<OnChannelLeave>(context::isForMyCharacter)

    inner class Actions {
        fun updatePublicChannels() = context.send(PublicChannelListRequest())
        fun updatePrivateChannels() = context.send(PrivateChannelListRequest())

        fun joinPublic(title: String) = joinPrivate(title)
        fun joinPublic(channel: PublicChannelRecord) =  joinPublic(channel.name)

        fun joinPrivate(identifier: String) = context.send(ChannelJoinRequest(identifier))
        fun joinPrivate(channel: PrivateChannelRecord) = joinPrivate(channel.identifier)
    }

    override var disposables: MutableList<Disposable> = onInitialization()
    override fun onInitialization() : MutableList<Disposable> {
        return mutableListOf(
                packets onType PacketHandler()::publicChannelList,
                packets onType PacketHandler()::privateChannelList,
                packets onType PacketHandler()::joinChannel,
                channelLeave onEvent EventHandler()::channelLeft
        )
    }

    override fun onClientConnect() {
       //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onClientDisconnect() {
        channels.onEach(Disposable::dispose)
        channels.clear()
    }

    override fun onClientError() {
        channels.onEach(Disposable::dispose)
        channels.clear()
        dispose()
    }

    private inner class PacketHandler {
        fun publicChannelList(packet: ChannelListPublic) {
            if(publicChannels.isEmpty())
                packet.makeRecords().onEach { publicChannels[it.name] = it }
            else
            {
                packet.makeRecords().onEach {
                    when (val record = publicChannels[it.name]) {
                        null -> publicChannels[it.name] = it
                        else -> {
                            record.lastUpdated = Instant.now()
                            record.count = it.count
                        }
                    }
                }
            }
        }

        fun privateChannelList(packet: ChannelListPrivate) {
            if(privateChannels.isEmpty())
                packet.makeRecords().onEach { privateChannels[it.name] = it }
            else
            {
                packet.makeRecords().onEach {
                    when (val record = privateChannels[it.name]) {
                        null -> privateChannels[it.name] = it
                        else -> {
                            record.lastUpdated = Instant.now()
                            record.count = it.count
                        }
                    }
                }
            }
        }

        fun joinChannel(packet: JCH) {
            if(packet.character.identity == context.character) {
                logger.info { "Received Join Packet for '${packet.title}'" }

                var cr: ChannelContext? = null

                if(packet.title.startsWith("ADH")) {
                    val cid = privateChannels[packet.title]

                    if(cid != null)
                        cr = ChannelContext(context, cid.name, cid.identifier)
                    else
                        logger.error { "Issue looking up channel identity, indexes lacking or not-initialized." }
                }
                else
                    cr = ChannelContext(context, packet.title)


                if(cr != null) {
                    // TODO: Test if handling this in the channel context works better than here, clearer separation of concerns.

                    //context.triggerEvent(OnChannelJoined(context, cr, context.getCharacter()))
                    channels.add(cr)
                }
            }
        }
    }

    private inner class EventHandler {
        fun channelLeft(onChannelLeave: OnChannelLeave) {
            onChannelLeave.whenCharacter(context.getCharacter()) {
                val chn = channel
                channels.remove(chn)
                chn.dispose()
            }
        }
    }
}

private fun ChannelListPublic.makeRecords() = channels.map(::PublicChannelRecord)
private fun ChannelListPrivate.makeRecords() = channels.map(::PrivateChannelRecord)

data class PublicChannelRecord(val name: String, var mode: ChannelMode, var count: Int) {
    internal constructor(record: InternalPublicChannel) : this(record.name, record.mode, record.characters)

    var lastUpdated: Instant = Instant.now()
}
data class PrivateChannelRecord(val name: String, val identifier: String, var count: Int) {
    internal constructor(record: InternalPrivateChannel) : this(record.title, record.name, record.characters)

    var lastUpdated: Instant = Instant.now()
}
package flist.root.ktchat.channel

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider

enum class ChannelMode {
    Chat, RP, Both
}

class ChannelModeDeserializer : JsonDeserializer<ChannelMode>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): ChannelMode {
        return when(p?.valueAsString) {
            "both" -> ChannelMode.Both
            "chat" -> ChannelMode.Chat
            else -> ChannelMode.RP
        }
    }
}

class ChannelModeSerializer : JsonSerializer<ChannelMode>() {
    override fun serialize(value: ChannelMode?, gen: JsonGenerator?, serializers: SerializerProvider?) {
        gen?.writeString(value?.name?.toLowerCase())
    }
}
package flist.root.ktchat.di

import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier
import org.koin.core.scope.Scope
import kotlin.reflect.KClass

interface ScopedKoinComponent {
    /**
     * Get the associated Koin instance
     */
    val scope: Scope
}

/**
 * Get instance instance from Koin
 * @param qualifier
 * @param parameters
 */
inline fun <reified T> ScopedKoinComponent.get(
        qualifier: Qualifier? = null,
        noinline parameters: ParametersDefinition? = null
): T = scope.get(qualifier, parameters)

/**
 * Lazy inject instance from Koin
 * @param qualifier
 * @param parameters
 */
inline fun <reified T> ScopedKoinComponent.inject(
        qualifier: Qualifier? = null,
        noinline parameters: ParametersDefinition? = null
): Lazy<T> = scope.inject(qualifier, parameters)

/**
 * Get instance instance from Koin by Primary Type P, as secondary type S
 * @param parameters
 */
inline fun <reified S, reified P> ScopedKoinComponent.bind(
        noinline parameters: ParametersDefinition? = null
): S = scope.bind<S, P>(parameters)

inline fun <reified T> ScopedKoinComponent.declare(
        instance: T,
        qualifier: Qualifier? = null,
        secondaryTypes: List<KClass<*>>? = null
) = scope.declare(instance, qualifier, secondaryTypes)
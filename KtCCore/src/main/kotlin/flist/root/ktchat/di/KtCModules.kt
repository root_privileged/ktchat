package flist.root.ktchat.di

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.tinder.scarlet.Protocol
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.websocket.ShutdownReason
import com.tinder.scarlet.websocket.okhttp.OkHttpWebSocket
import flist.root.ktchat.channel.ChannelDirectory
import flist.root.ktchat.character.CharacterDirectory
import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.di.KtCModules.Properties.ChatURL
import flist.root.ktchat.di.KtCModules.Properties.Name
import flist.root.ktchat.di.KtCModules.Properties.Version
import flist.root.ktchat.di.KtCModules.Scopes.AccountScope
import flist.root.ktchat.di.KtCModules.Scopes.ChatEventScope
import flist.root.ktchat.di.KtCModules.Scopes.ChatScope
import flist.root.ktchat.di.KtCModules.Scopes.EventEmit
import flist.root.ktchat.di.KtCModules.Scopes.GlobalEventScheduler
import flist.root.ktchat.di.KtCModules.Scopes.GlobalEventScope
import flist.root.ktchat.di.KtCModules.Scopes.PacketEmit
import flist.root.ktchat.event.EventEmitter
import flist.root.ktchat.internal.connection.ChatSocketLifecycle
import flist.root.ktchat.internal.connection.FChatSocketService
import flist.root.ktchat.internal.connection.ReactorStreamAdapterFactory
import flist.root.ktchat.internal.connection.ServerPacketMessageAdapter
import flist.root.ktchat.internal.jackson.KtCJacksonModule
import flist.root.ktchat.internal.jackson.KtCJacksonSerializer
import flist.root.ktchat.internal.packets.server.PacketEmitter
import flist.root.ktchat.site.AccountContext
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.features.json.JsonFeature
import kotlinx.coroutines.asCoroutineDispatcher
import okhttp3.OkHttpClient
import okhttp3.Request
import org.koin.core.context.GlobalContext
import org.koin.core.context.startKoin
import org.koin.core.qualifier.named
import org.koin.dsl.module
import reactor.core.scheduler.Schedulers
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

object KtCModules {
    fun initialize() = startKoin {
        modules(allModules)

        properties(mapOf(
                ChatURL to "wss://chat.f-list.net/chat2",
                Name to "KtCCore",
                Version to "1.0.0-Incubating"
        ))
    }

    object Properties {
        const val ChatURL = "chaturl"
        const val Name = "clientName"
        const val Version = "clientVersion"
    }

    object Scopes {
        val AccountScope = named("account")
        val ChatScope = named("chat")

        val GlobalEventScope = named("globalEventScope")
        val GlobalEventScheduler = named("globalEventScheduler")
        val ChatEventScope = named("chatEventScope")

        val EventEmit = named("EventEmitter")
        val PacketEmit = named("PacketEmitter")
    }

    private val globalModule = module {

        single {
            jacksonObjectMapper().apply {
                disable(SerializationFeature.INDENT_OUTPUT)
                enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
                registerModule(KotlinModule())
                registerModule(JavaTimeModule())
                registerModule(KtCJacksonModule())
            }
        }

        single {
            OkHttpClient.Builder()
                    .build()
        }

        single {
            HttpClient(OkHttp) {
                //install(WebSockets) {}
                install(JsonFeature) {
                    serializer = KtCJacksonSerializer(get())

                }

                engine { preconfigured = get() }
            }
        }

        single<Protocol> {
            OkHttpWebSocket(get(), OkHttpWebSocket.SimpleRequestFactory(
                    { Request.Builder().url(getProperty<String>(ChatURL)).build()},
                    { ShutdownReason.GRACEFUL }
            ))
        }

        single<ExecutorService> {
            Executors.newWorkStealingPool()
        }

        single(GlobalEventScope) {
            get<ExecutorService>().asCoroutineDispatcher()
        }

        single(GlobalEventScheduler) {
            Schedulers.fromExecutorService(get<ExecutorService>())
        }

        factory { (name: String, password: String) -> AccountContext(GlobalContext.get().koin, name, password) }
    }

    private val accountModule = module {
        scope(AccountScope) {
            factory {
                (character: String) -> ChatContext(get<AccountContext>().scope, character)
            }
        }
    }

    private val chatModule = module {
        scope(ChatScope) {
            scoped<Scarlet.Configuration> {
                Scarlet.Configuration(
                        lifecycle = get<ChatSocketLifecycle>(),
                        messageAdapterFactories = listOf(ServerPacketMessageAdapter.Factory()),
                        streamAdapterFactories = listOf(ReactorStreamAdapterFactory()),
                        debug = true
                )
            }

            scoped<ChatSocketLifecycle> {
                ChatSocketLifecycle()
            }

            scoped<Scarlet> {
                Scarlet(get(), get())
            }

            scoped<FChatSocketService> {
                get<Scarlet>().create()
            }

            scoped<EventEmitter>(EventEmit) { EventEmitter.create() }
            scoped<PacketEmitter>(PacketEmit) { PacketEmitter.create() }

            scoped { CharacterDirectory(get()) }
            scoped { ChannelDirectory(get()) }

            scoped<Executor> { Executors.newWorkStealingPool(1) }
            scoped(ChatEventScope) { get<Executor>().asCoroutineDispatcher() }
            scoped { Schedulers.fromExecutor(get(), true) }
        }
    }

    private val allModules = globalModule + accountModule + chatModule
}

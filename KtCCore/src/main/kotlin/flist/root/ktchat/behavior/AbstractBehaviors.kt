package flist.root.ktchat.behavior

import flist.root.ktchat.channel.ChannelContext
import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.chat.ContextAware
import flist.root.ktchat.event.AbstractChannelEvent
import flist.root.ktchat.event.AbstractEvent
import flist.root.ktchat.event.ChannelEvents
import flist.root.ktchat.event.EventFlux
import flist.root.ktchat.internal.packets.server.AbstractChannelPacket
import flist.root.ktchat.internal.packets.server.AbstractServerPacket
import flist.root.ktchat.internal.packets.server.ChannelPacketFlux
import flist.root.ktchat.internal.packets.server.PacketFlux

abstract class AbstractChatBehavior(
        override val context: ChatContext,
        override val events: EventFlux = context.events,
        override val packets: PacketFlux = context.packets
): ContextAware<AbstractEvent, AbstractServerPacket> {
    internal fun triggerEvent(event: AbstractEvent) = context.triggerEvent(event)
}

abstract class AbstractChannelBehavior(
        private val channelContext: ChannelContext,
        override val context: ChatContext = channelContext.chatContext,
        override val events: ChannelEvents = channelContext.events,
        override val packets: ChannelPacketFlux = channelContext.packets
) : ContextAware<AbstractChannelEvent, AbstractChannelPacket> {
    internal fun triggerEvent(event: AbstractEvent) = context.triggerEvent(event)
}
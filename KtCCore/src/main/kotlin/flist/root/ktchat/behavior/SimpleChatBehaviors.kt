package flist.root.ktchat.behavior

import flist.root.ktchat.chat.ChatContext
import flist.root.ktchat.event.OnRawPacket
import flist.root.ktchat.internal.packets.client.PingRequest
import flist.root.ktchat.internal.packets.server.*
import mu.KotlinLogging
import reactor.core.Disposable

private val logger = KotlinLogging.logger {}

sealed class SimpleChatBehaviors(context: ChatContext) : AbstractChatBehavior(context) {
    override var disposables: MutableList<Disposable> = onInitialization()

    final override fun onInitialization(): MutableList<Disposable> = mutableListOf(
            packets onType ::uptimePacket,
            packets onType ::errorPacket,
            packets onType ::helloPacket,
            packets onType ::parseErrorPacket,
            packets onType ::ping,
            packets onType ::serverVariable
    )

    override fun onClientConnect() {

    }

    override fun onClientDisconnect() {

    }

    override fun onClientError() {
    }

    private fun parseErrorPacket(packet: RawPacket) {
        logger.error { "Received Raw Packet $packet" }
        triggerEvent(OnRawPacket(context, packet))
    }

    private fun errorPacket(packet: Error) {
        logger.info { "Server Error: $packet" }
    }

    private fun helloPacket(packet: ServerHello) {
        logger.info { "Server says ${packet.message}" }
    }

    private fun uptimePacket(packet: UptimeNotification) {
        logger.info { "Server Uptime Message: $packet"}
    }

    private fun serverVariable(packet: ServerVariable) {
        logger.info{ "Server Variable ${packet.variable} received." }
        context.serverVariables[packet.variable] = packet.unknownProperties["value"]!!
    }

    @Suppress("UNUSED_PARAMETER")
    private fun ping(packet: Ping) {
        logger.trace("Pong.")
        context.send(PingRequest())
    }
}
package flist.root.ktchat

object TestData {
    val ServerVariable_Packets = listOf(
            """VAR {"innerValue":4096,"variable":"chat_max"}""",
            """VAR {"innerValue":50000,"variable":"priv_max"}""",
            """VAR {"innerValue":50000,"variable":"lfrp_max"}""",
            """VAR {"innerValue":50000,"variable":"cds_max"}""",
            """VAR {"innerValue":600,"variable":"lfrp_flood"}""",
            """VAR {"innerValue":0.5,"variable":"msg_flood"}""",
            """VAR {"innerValue":5,"variable":"sta_flood"}""",
            """VAR {"innerValue":"0","variable":"permissions"}""",
            """VAR {"innerValue":["frontpage","sex driven lfrp","story driven lfrp"],"variable":"icon_blacklist"}"""
    )

    const val HLO_Packet = """HLO {"message":"Welcome. Running F-Chat (0.10.0-Lua). Enjoy your stay."}"""
    const val CON_Packet = """CON {"count":8472}"""
    const val FRL_Packet = """FRL {"characters":["Somebody once told me","The world was gonna roll me","I ain't the sharpest","Tool in the shed..."]}	"""

    const val IGN_Packet = """IGN {"action":"init","characters":["Some people","Really are just","Shitty People"]}"""

}
package flist.root.ktchat

import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe


class KtCCoreSpek : Spek({

    beforeEachTest {
        println("Testing.... Go.")
    }

    describe("Event Subclass") {
        it("OnCharacterConnect is AbstractEvent")
        {
            assert("s" is String)
        }
    }
})

/*
@DisplayName("Flux Router Tests")
class KtCCoreSpek {

    @BeforeEach
    fun setUp() {
        println("Setting Up....")
    }

    @Test
    @DisplayName("Packet Construction Test")
    fun testPacketConversion() {
        val t = ""
    }

    @AfterEach
    fun tearDown() {
    }

    /*
    companion object {
        @BeforeAll
        fun initializeFlux() {
            KtCCore.inititalizeCore()

            assertTrue(AbstractPacket.serverPacketIndex.isNotEmpty(), "Server Packets not Initialized.")
            println("Packets Loaded: " + AbstractPacket.serverPacketIndex.size)
        }

        @AfterAll
        fun destroyFlux() {

        }
    } */
}
*/
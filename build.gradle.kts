buildscript {
    repositories {
        jcenter()
        maven {
            setUrl("https://dl.bintray.com/hotkeytlt/maven")
        }
        
        
    }
}
plugins {
    java
    kotlin("jvm") version DependencyVersions.ktVersion
}

repositories {
    jcenter()
}

project(":KtCCore")
project(":KtCScript")
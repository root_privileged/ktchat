object DependencyVersions {
    const val ktVersion = "1.4.21"

    const val spekVersion = "2.0.5"
    const val kluentVersion = "1.53"

    const val reactorBOM = "Californium-SR9"

    const val reflectionsVersion = "0.9.11"
    const val guavaVersion = "28.0-jre"
    const val javassistVersion = "3.25.0-GA"

    const val jacksonVersion = "2.9.9"
    const val jsoupVersion = "1.12.1"
    const val betterparseVersion = "0.3.5"

    const val ktorVersion = "1.2.3"
    const val scarletVersion = "0.2.5-SNAPSHOT"

    const val kotLoggingVersion = "1.6.26"
    const val log4j2Version = "2.12.1"

    const val graalJsVersion = "19.1.1"

    const val koinVersion = "2.0.1"

    const val tornadoFXVersion = "2.0.0-SNAPSHOT"
    //const val miglayoutVersion = "5.3-20190916.220209-288"
    const val miglayoutVersion = "5.3-SNAPSHOT"

    const val konfig = "1.6.10.0"
}

object Cfg {
    const val Koin = "org.koin:koin-core:${DependencyVersions.koinVersion}"
    const val KoinExt = "org.koin:koin-core-ext:${DependencyVersions.koinVersion}"

    const val GraalJS = "org.graalvm.js:js:${DependencyVersions.graalJsVersion}"

    const val BetterParse = "com.github.h0tk3y.betterParse:better-parse:${DependencyVersions.betterparseVersion}"

    const val Konfig = "com.natpryce:konfig:${DependencyVersions.konfig}"

    object UI {
        const val FXVersion = "12.0.1"
        const val TornadoFX = "no.tornado:tornadofx:${DependencyVersions.tornadoFXVersion}"
        const val MiglayoutFX = "com.miglayout:miglayout-javafx:${DependencyVersions.miglayoutVersion}"
    }

    object Testing {
        const val Spek = "org.spekframework.spek2:spek-dsl-jvm:${DependencyVersions.spekVersion}"
        const val SpekJUnitRunner = "org.spekframework.spek2:spek-runner-junit5:${DependencyVersions.spekVersion}"
        const val Kluent = "org.amshove.kluent:kluent:${DependencyVersions.kluentVersion}"
        const val Reactor = "io.projectreactor:reactor-test"

        const val KTor = "io.ktor:ktor-client-mock:${DependencyVersions.ktorVersion}"
    }

    object Web {
        const val JSoup = "org.jsoup:jsoup:${DependencyVersions.jsoupVersion}"

        const val Scarlet = "com.tinder.scarlet:scarlet:${DependencyVersions.scarletVersion}"
        const val ScarletOkHttp = "com.tinder.scarlet:protocol-websocket-okhttp:${DependencyVersions.scarletVersion}"

        const val KTor = "io.ktor:ktor:${DependencyVersions.ktorVersion}"
        const val KTorWebsocket = "io.ktor:ktor-client-websockets:${DependencyVersions.ktorVersion}"
        const val KTorJackson = "io.ktor:ktor-client-jackson:${DependencyVersions.ktorVersion}"
        const val KTorApache = "io.ktor:ktor-client-apache:${DependencyVersions.ktorVersion}"
        const val KTorOkHttp = "io.ktor:ktor-client-okhttp:${DependencyVersions.ktorVersion}"
    }

    object Logging {
        const val KotlinLogging = "io.github.microutils:kotlin-logging:${DependencyVersions.kotLoggingVersion}"
        const val Log4J2Api = "org.apache.logging.log4j:log4j-api:${DependencyVersions.log4j2Version}"
        const val Log4J2Core = "org.apache.logging.log4j:log4j-core:${DependencyVersions.log4j2Version}"
        const val Log4J2Impl = "org.apache.logging.log4j:log4j-slf4j-impl:${DependencyVersions.log4j2Version}"
    }

    object CommonLibraries {
        const val Reflections = "org.reflections:reflections:${DependencyVersions.reflectionsVersion}"
        const val Guava = "com.google.guava:guava:${DependencyVersions.guavaVersion}"
        const val Javassist = "org.javassist:javassist:${DependencyVersions.javassistVersion}"


        object Reactor {
            const val BOM = "io.projectreactor:reactor-bom:${DependencyVersions.reactorBOM}"
            const val Core = "io.projectreactor:reactor-core"
            const val Extras = "io.projectreactor.addons:reactor-extra"
        }

        object Jackson {
            const val BOM = "com.fasterxml.jackson:jackson-bom:${DependencyVersions.jacksonVersion}"
            const val Core = "com.fasterxml.jackson.core:jackson-core"
            const val Databind = "com.fasterxml.jackson.core:jackson-databind"
            const val JSR310 = "com.fasterxml.jackson.datatype:jackson-datatype-jsr310"
            const val Annotations = "com.fasterxml.jackson.core:jackson-annotations"
            const val YAML = "com.fasterxml.jackson.dataformat:jackson-dataformat-yaml"
        }
    }
}